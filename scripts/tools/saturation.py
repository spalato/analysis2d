#!/bin/python
# saturation.py: prepare saturation report

# produces a file. The header contains descriptive data
# the rest is a np.loadtxt file with number of saturated pixels.

from __future__ import division, print_function
from glob import glob
import argparse
import logging
import numpy as np
import json
import os.path as pth
from analysis2d.utils import is_stab_wave, load_exp_config


parser = argparse.ArgumentParser(
    description="Analyze saturated pixels.",
    epilog="""Write a report about staturated pixels. Contains a report in the
    header and saturated pixel counts as an array that can be loaded using numpy.loadtxt.
    
    The array has shape (M,N) for M files and N waves in a `sequence_rep_ZZZ.npy`.
    
    The header is a JSON document, lines indicated by '#'"""
)
parser.add_argument("-o", "--output", action="store", default=None,
                    help="Output file name. Defaults to 'saturation.txt', in same folder as first sequence file.")
parser.add_argument("-l", "--log", action="store", default=None,
                    help="Redirect logging to file.")
parser.add_argument("settings_file", help="Settings file path")
parser.add_argument("in_files", metavar="sequence", nargs="+",
                    help="Input sequence file name. Accepts patterns.")
# parser.add_argument("timestamps_files", metavar="timestamps", nargs=1,
#                     help="Input timestamps file name. Accepts patterns.")
parser.add_argument("-v", "--verbose", action='store_true',
                    help="Set logging level to DEBUG")
args = parser.parse_args()

loglev = logging.DEBUG if args.verbose else logging.INFO
log_kw = {"level": loglev,
          "format": "%(levelname)-7s  %(message)s"}
if args.log:
    log_kw['filename'] = args.log
logging.basicConfig(**log_kw)
logging.captureWarnings(True)

logging.info("Loading settings: {}".format(args.settings_file))
cfg, t1, n_rep = load_exp_config(args.settings_file)
inp = []
[inp.extend(glob(f)) for f in args.in_files]

counts_threshold = 65000  # counts per pix
wl_threshold = 5 # pixels per spectrum
wave_threshold = 2 # waves per repeat

#dat = (np.load(f) for f in inp)
sat_count = []
for i, f in enumerate(inp):
    logging.debug("Processing: {}".format(f))
    dat = np.load(f)
    if i == 0:
        m = is_stab_wave(dat.shape[-1], n_rep)
    sat_count.append(np.count_nonzero(dat > counts_threshold, axis=0).astype("uint32"))
sat_count = np.array(sat_count)
assert sat_count.shape == (len(inp), m.size)
stab_w = sat_count.compress(m, axis=1)
rej_stab = np.count_nonzero(stab_w[:,1:] > wl_threshold, axis=1)
data_w = sat_count.compress(~m, axis=1)
rej_data = np.count_nonzero(data_w > wl_threshold, axis=1)
rej_waves = rej_data > wave_threshold

hdr = {
    "n_files": len(inp),
    "Total rejected stab": int(np.sum(rej_stab)),
    "Total rejected data": int(np.sum(rej_data)),
    "Total rejected waves": int(np.sum(rej_waves)),
    "Rejected reps": np.flatnonzero(rej_waves).tolist(),
}
hdr = json.dumps(hdr, indent=2)

out = pth.join(pth.dirname(inp[0]), "saturations.txt") if args.output is None else args.output
logging.info("Saving to: {}".format(out))
np.savetxt(out, sat_count, fmt="%d", header=hdr)
from pprint import pprint, pformat
#print(pformat(hdr)[1:-1])


