#!/bin/python
# sorted spectra: print list of spectra directory, sorted numerically.
# usage: python sorted_spectra [folder] [folder] [folder...]

import sys
from analysis2d.utils import spectra_idx

paths = sys.argv[1:]

for p in sorted(paths, key=spectra_idx):
    print(p)
