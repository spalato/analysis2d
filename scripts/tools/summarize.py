#!/bin/python
# summarize.py : summarize experiments to a common file.
# not made for batch processing: no logging used.

from __future__ import division, print_function
import argparse
from glob import glob
import os.path as pth
from analysis2d.utils import load_exp_config

parser = argparse.ArgumentParser(description="Summarize experiment settings to a common file.")
parser.add_argument("-o", "--output", default="exp_summary.txt",
                    help="Output file name. Defaults to exp_summary.txt")
parser.add_argument("--overwrite", action='store_true',
                    help='Overwrite output file.')
parser.add_argument("in_files", metavar="settings", nargs='+',
                    help="Input settings.cfg files.")
args = parser.parse_args()

hdr = ['path', 'type', 'n_rep', 'n_spec', 'comment']
hdr = '\t'.join(["{:15s}", "{:18s}", "{:4s}", "{:4s}", "{:s}"]).format(*hdr)
hdr += '\n' + '-' * len(hdr.expandtabs()) + '\n'
fmt = '\t'.join(["{:15s}", "{:18s}", "{:d}", "{:d}","{:s}"])+'\n'


inp = []
[inp.extend(glob(f)) for f in args.in_files]

if not pth.isfile(args.output) or args.overwrite:
    with open(args.output, 'w') as f:
        f.write(hdr)

with open(args.output, 'a') as out:
    for f in inp:
        params, _, _ = load_exp_config(f)
        total_rep = params['n_rep']
        try:
            total_rep *= params['n_exp']
        except KeyError:  # old format
            pass
        n_spec = len(glob(pth.dirname(pth.abspath(f))+"/spectrum_*/"))
        try:
            comment = params['comment']
        except KeyError: # old format
            comment =  ''
        out.write(fmt.format(
            pth.dirname(pth.normpath(f)),
            params['experiment_type'],
            total_rep,
            n_spec,
            comment))