# convert data to coherences using 4 step phase cycling (2x2x1)
# usage: cycle4step.py <data>

from __future__ import division, print_function
import sys
import os.path as pth
import numpy as np
import logging
logging.basicConfig(level=logging.INFO,
                    format="%(filename)-16s %(levelname)-7s - %(message)s")

fn = sys.argv[1]
logging.info("Opening: {}".format(fn))

dset = np.load(fn)
t1 = dset['t1']
t2 = dset['t2']
wl = dset['wl']
data = dset['data']


assert data.shape == (t1.size*4, t2.size, wl.size)
coh = - data[::4,:,:] + data[1::4,:,:] + data[2::4,:,:] - data[3::4,:,:]

items = dict(dset.items())
items['coh'] = coh
logging.info("Saving to: {}".format(fn))
np.savez_compressed(fn, **items)
