# add_norms.py  add norm waves to the data stack
# usage: python add_norm.py [norm_waves.txt ...] [data_stack.npz]

from __future__ import division, print_function
import sys
import numpy as np
from glob import glob
import os.path as pth
import logging
import argparse
from analysis2d.utils import spectra_idx
logging.basicConfig(level=logging.INFO,
                    format="%(filename)-16s %(levelname)-7s - %(message)s")

parser = argparse.ArgumentParser(
    description="Add normalised data waves to data.npz archive",
    epilog="Adds 'data_n' key, containing normalised data waves."
)
parser.add_argument("dataset", help="Path to archive")
parser.add_argument("norm_waves", metavar="snorm", nargs="+",
                    help="Path of norm waves files. Accepts patterns.")

a = parser.parse_args()
dset_fn = a.dataset
norm_fns = a.norm_waves

logging.info("Opening: {}".format(dset_fn))
dset = np.load(dset_fn)
wl = dset["wl"]

norm_fns = [f for p in norm_fns for f in glob(p)] # go figure.
norm_fns = list(sorted(norm_fns,
                key=spectra_idx))
#from pprint import pprint
#pprint(norm_fns)
norms = []
for sfn in norm_fns:
    logging.info("Adding normwaves from: {}".format(sfn))
    norm = np.loadtxt(sfn)
    assert norm.shape[0] == wl.size
    assert np.allclose(wl, norm[:,0])
    norms.append(norm[:,1:])
norms = np.array(norms)
norms = np.rollaxis(norms,-1,0) #Roll (t2,wl,t1) -> (t1,t2,wl)
logging.info("norms.shape: {}".format(norms.shape))
assert norms.shape == dset["data"].shape
items = dict(dset.items())
items["data_n"] = norms
logging.info("Saving to: {}".format(dset_fn))
np.savez_compressed(dset_fn, **items)
