# add_stab.py  add stab waves to the data stack
# usage: python add_stab.py [stab_waves.txt ...] [data_stack.npz]

from __future__ import division, print_function
import sys
import numpy as np
from glob import glob
import os.path as pth
import logging
import argparse
from analysis2d.utils import spectra_idx
logging.basicConfig(level=logging.INFO,
                    format="%(filename)-16s %(levelname)-7s - %(message)s")

parser = argparse.ArgumentParser(
    description="Add stab waves to data.npz archive",
    epilog="Adds 'stab' key, containing averaged stab waves, skipping first."
)
parser.add_argument("dataset", help="Path to archive")
parser.add_argument("stab_waves", metavar="stab", nargs="+",
                    help="Path of stab waves files. Accepts patterns.")

a = parser.parse_args()
dset_fn = a.dataset
stab_fns = a.stab_waves

logging.info("Opening: {}".format(dset_fn))
dset = np.load(dset_fn)
wl = dset["wl"]

stab_fns = [f for p in stab_fns for f in glob(p)] # go figure.
stab_fns = list(sorted(stab_fns,
                key=spectra_idx))
#from pprint import pprint
#pprint(stab_fns)
stabs = []
for sfn in stab_fns:
    logging.info("Adding stabwaves from: {}".format(sfn))
    stab = np.loadtxt(sfn)
    assert stab.shape[0] == wl.size
    assert np.allclose(wl, stab[:,0])
    stabs.append(np.mean(stab[:,1:], axis=1))
stabs = np.array(stabs)

#logging.info("stabs.shape: {}".format(stabs.shape))
assert stabs.shape == (dset["t2"].size, wl.size)
items = dict(dset.items())
items["stab"] = stabs
logging.info("Saving to: {}".format(dset_fn))
np.savez_compressed(dset_fn, **items)
