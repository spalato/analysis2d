# convert data to coherences using 2step phase cycling
# usage: cycle2step.py data

from __future__ import division, print_function
import sys
import os.path as pth
import numpy as np

fn = sys.argv[1]
print("Processing", fn)

dset = np.load(fn)
t1 = dset['t1']
t2 = dset['t2']
wl = dset['wl']
data = dset['data']


assert data.shape == (t1.size*2, t2.size, wl.size)
coh = data[::2,:,:] - data[1::2,:,:]

items = dict(dset.items())
items['coh'] = coh

np.savez_compressed(fn, **items)
