# build volumetric stack of the dataset, t1, t2, wl
# usage: python vol_stack.py <folder> <data_pattern> <outf>

from __future__ import division, print_function
import sys
import numpy as np
from glob import glob
import os.path as pth
from analysis2d.utils import load_exp_config, spectra_idx
from tqdm import tqdm
import logging
import argparse
logging.basicConfig(level=logging.INFO,
                    format="%(filename)-16s %(levelname)-7s - %(message)s")

# TODO: test updated parser...
parser = argparse.ArgumentParser(
    description="Collect data waves on a 3d array (volumetric stack)",
    epilog="Contents is: t1, t2, wl and data. Data has shape (t1.size, t2.size, wl.size)"
)
parser.add_argument("-o", "--output", default="stack_data_.npz",
                    help="Output file name. Defaults to 'stack_data_.npz'")
parser.add_argument("settings", help="Settings file path")
parser.add_argument("data_files", metavar="data", nargs="+",
                    help="Glob pattern for data files.")

a = parser.parse_args()

#root, data_pattern, outn = sys.argv[1:]

params, t1, n_rep = load_exp_config(a.settings)
t2 = np.array(params['t2'])
for k, v in params.items():
    logging.debug(k, v)
data_files = [f for p in a.data_files for f in glob(p)] # go figure.
data_files = list(sorted(data_files,
                    key=spectra_idx))
#pprint(data_files)
# spectrum x, wl, shots
data = np.array([np.loadtxt(f) for f in tqdm(data_files, desc=pth.basename(__file__)+' - Loading data')])

wl_all = data[:,:,0]
wl = wl_all[0].copy()
assert np.allclose(wl_all - wl, 0)  # all wl arrays are the same
data = np.rollaxis(data[:,:,1:], -1, 0) # roll (t2, w_sig, t1) -> (t1, t2, w_sig)

logging.debug("data shape: {}".format(data.shape))
logging.debug("t1, t2, wl:  {}, {}, {}".format(t1.size, t2.size, wl.size))

logging.info("Saving to: {}".format(a.output))
np.savez_compressed(a.output, data=data, t1=t1, t2=t2, wl=wl)
