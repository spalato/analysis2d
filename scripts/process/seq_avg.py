#!/bin/python
# seq_avg.py: average sequences from 2D setup (2D, pp...)

from __future__ import division, print_function
from glob import glob
import os.path as pth
import argparse
import numpy as np
from analysis2d.utils import load_sequences
import logging

parser = argparse.ArgumentParser(description="Average sequences files.")
parser.add_argument("-o", "--output", action="store", default=None,
                    help="Output file name. Defaults to 'sequence_mean.npy', in same folder as first sequence file.")
parser.add_argument("-l", "--log", action="store", default=None,
                    help="Redirect logging to file.")
parser.add_argument("in_files", metavar="sequence", nargs="+",
                    help="Input sequence file name. Accepts patterns.")
parser.add_argument("timestamps_files", metavar="timestamps", nargs=1,
                    help="Input timestamps file name. Accepts patterns.")
parser.add_argument("-v", "--verbose", action='store_true',
                    help="Set logging level to DEBUG")
args = parser.parse_args()

loglev = logging.DEBUG if args.verbose else logging.INFO
log_kw = {"level": loglev,
          "format": "{:16s}".format(pth.basename(__file__))+" %(levelname)-7s - %(message)s"}
if args.log:
    log_kw['filename'] = args.log
logging.basicConfig(**log_kw)
logging.captureWarnings(True)

inp = []
[inp.extend(glob(f)) for f in args.in_files]

ts_inp=[]
[ts_inp.extend(glob(f)) for f in args.timestamps_files]
try:
    data_mean = load_sequences(inp, ts_inp, pbar="{:s} loading".format(pth.basename(__file__)))
except:
    logging.exception("Exception encountered while processing {}".format(args.in_files))
    raise

if args.output is None:
    args.output = pth.join(pth.dirname(inp[0]), "sequence_mean.npy")

logging.info("Saving to: {}".format(args.output))
if args.output.endswith('.npy'):
    np.save(args.output, data_mean)
else:
    np.savetxt(args.output, data_mean, fmt='%.06e')
