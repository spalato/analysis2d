#!/bin/bash

trial=${PWD##*/}  # This is parent folder name, eg: trial_05
for n in 0 2   # $(seq 0 10)
do
	rep=$(printf "rep_%03d" $n)  # Pad to 3 digits: 001
    # automatic output filenames, eg: trial_05_data_rep_000.npz
    data_stack_name=${trial}_data_${rep}.npz
    spec_stack_name=${trial}_spec_${rep}.npz
	sequence_pat="*/sequence_${rep}.npy" # all sequence_rep
	# average data and stab waves for each spectrum
	for f in $(ls ${sequence_pat})
	do
	    dir=${f%/*}
		python wfpack_avg.py -o ${dir}/data_${rep}.txt settings.cfg $f
		python stabwaves_avg.py -o ${dir}/stab_${rep}.txt settings.cfg $f
	done
    # create volumetric stack for data

	python vol_stack.py -o ${data_stack_name} settings.cfg "*/data_${rep}.txt"
	# add stab waves to the volumetric stack
	python add_stab.py ${data_stack_name} "*/stab_${rep}.txt"
	# apply 4step phase cycling.
    # Other scripts are required for other phase cycling schemes
	python cycle4step.py ${data_stack_name}	
	# Compute 2d spectra stack (weee)
	python coh2spec.py ${data_stack_name} ${spec_stack_name}
	# Apply rotating frame and clip values of E1 beyond spectrometer range.
	python shift_clip.py -s settings.cfg 0.1 ${spec_stack_name}
done

