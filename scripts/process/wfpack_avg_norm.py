#!/bin/python
# wfpack_avg_normed.py : average waveform packs with normalization each wfpack

from __future__ import division, print_function
import argparse
import os.path as pth
import numpy as np
from analysis2d.utils import acton_wl, load_exp_config, seqrep2data
import logging
import sys
import traceback

parser = argparse.ArgumentParser(description="Split, chunk and average a sequence. Includes wavelength values in final file.",
                                 epilog="Defaults wl come from global wl file in analysis2d module.")
parser.add_argument("-v", "--verbose", action='store_true',
                    help="Set logging level to DEBUG")
parser.add_argument("-l", "--log", action="store", default=None,
                    help="Redirect logging to file.")
parser.add_argument("-o", "--output", default=None,
                    help="Output file name. Defaults to 'data_.txt'")
parser.add_argument("-r", "--roll", type=int, default=0,
                    help="Initial data by this number of spectra.")
parser.add_argument("-s", "--nstab", default=2, type=int,
                    help="Number of stab waves. Defaults to 2")
parser.add_argument("-w", "--nwaves", default=38, type=int,
                    help="Number of waveforms per pack. Defaults to 38")
parser.add_argument("--wl_file", default=None,
                    help="Acton wavelengths file path.")
parser.add_argument("settings",
                    help="Settings file path")
parser.add_argument("in_file", help="Sequence file path.")

a = parser.parse_args()

# log all exceptions
def log_excepthook(*exc):
    txt = "".join(traceback.format_exception(*exc))
    logging.critical("Exception while processing {} {}\n".format(a.settings, a.in_file)
                     +txt)
sys.excepthook = log_excepthook

loglev = logging.DEBUG if a.verbose else logging.INFO
log_kw = {"level": loglev,
          "format": "%(filename)-16s %(levelname)-7s - %(message)s"}
if a.log:
    log_kw['filename'] = a.log
logging.basicConfig(**log_kw)
logging.captureWarnings(True)

wl = acton_wl(fname=a.wl_file)
params, time, n_rep = load_exp_config(a.settings)
#logging.info("Opening: {}".format(a.in_file))
if a.in_file.endswith('.npy'):
    data = np.load(a.in_file)
else:
    data = np.loadtxt(a.in_file)
data = np.roll(data, a.roll, 1)
data = seqrep2data(data, n_rep, a.nwaves, a.nstab)
# Crashes when there are only two packs
out = np.ones((data.shape[0], data.shape[1]+1))*np.nan
out[:,0] = wl
out[:,1:] = data
if a.output is None:
    root = a.in_file.split('.')[-2]
    a.output = root+'_data_norm.txt'
#logging.info("Saving to {}".format(a.output))
np.savetxt(a.output, out, fmt='%.06e')
