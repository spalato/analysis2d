#!/bin/python
# shift_clip.py  apply frame shift and clip 2d spectra along e_ex
# usage: python shift_clip.py -s <settings.cfg> <fn>

from __future__ import division, print_function
import sys, os
import numpy as np
from scipy.constants import peta, eV, h, lambda2nu, nano
import logging
import re
import argparse
logging.basicConfig(level=logging.INFO,
                    format="%(filename)-16s %(levelname)-7s - %(message)s")

def f2e(f):
    return f*peta*h/eV

def nm2thz(wl):
    return lambda2nu(wl*nano)/peta

parser = argparse.ArgumentParser(description="Apply rotating frame to data.")
# then use mutually exclusive groups (add_mutually_exclusive_group(required=True))
ex_shift = parser.add_mutually_exclusive_group(required=True)
ex_shift.add_argument("-m", "--manual", action="store", nargs=2, type=float,
                      help="Central wavelength (nm) and user frame shift (PHz)")
ex_shift.add_argument("-s", "--settings", action="store",
                      help="Settings file")
parser.add_argument("file",
                    help="Spectra as .npz archive.")
args = parser.parse_args()

if args.settings is not None:
    with open(args.settings) as f:
        frame = float(re.findall(r"rot_frame_freq *= *([\d.]+)", f.read())[0])
else:
    cwl, user = args.manual
    frame = nm2thz(cwl)-user
fn = args.file

logging.info("Opening: {}".format(fn))
dset = np.load(fn)
e_ex = f2e(dset['f_ex']+frame)
e_em = f2e(dset['f_em'])
m = (e_ex >= np.min(e_em)) & (e_ex <= np.max(e_em))
e_ex = np.compress(m, e_ex)
buf_fn="sc_buf.tmp"
out = np.memmap(buf_fn, mode='w+', dtype='complex128',
                shape=(e_ex.size, dset['t2'].size, e_em.size))
dset['spec'].compress(m, axis=0, out=out)

items = dict(dset.items())
dset.close()
items['e_ex'] = e_ex
items['e_em'] = e_em
items['spec'] = out
np.savez_compressed(fn, **items)
del out
os.remove(buf_fn)
