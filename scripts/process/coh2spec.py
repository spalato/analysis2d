# coh2spec.py  Convert coherence to spectra, operates on volumes
# usage: python coh2spec.py input output
# computes the spectrum from the normalized coherences

from __future__ import division, print_function
import sys, os
import numpy as np
from analysis2d.utils import coh2spec
from tqdm import trange
import logging
logging.basicConfig(level=logging.INFO,
                    format="%(filename)-16s %(levelname)-7s - %(message)s")

in_f, out_f = sys.argv[1:3]
logging.info("Opening: {}".format(in_f))

dset = np.load(in_f)
t1 = dset['t1']
t2 = dset['t2']
wl = dset['wl']
coh = dset['coh']
stab = dset['stab']
assert coh.shape == (t1.size, t2.size, wl.size)
assert stab.shape == (t2.size, wl.size)
dset.close()
norm = stab - np.min(stab, axis=1)[:,np.newaxis]
scale = np.max(norm, axis=1)
thres = scale * 0.05

m = norm < thres[:, np.newaxis]
norm[m] = np.max(scale)*2
coh /= norm

# roll back t2 to first axis
#coh = np.rollaxis(coh, 1)
# do the job...
buf_fn = "temp.dat"
specs = np.memmap(buf_fn, mode='w+', dtype='complex128', shape=(t1.size*4, t2.size, wl.size))
for i in trange(t2.size):
    specs[:,i,:] = coh2spec(t1, wl, coh[:,i,:].T)[2].T
f_ex, f_em, _ = coh2spec(t1, wl, coh[:,i,:].T)

# clip: keep values up to max(f_em) (we don't counter rotate that much...)
#to_keep = np.abs(f_ex) < np.max(f_em)
#specs = np.compress(to_keep, specs, axis=0)
#f_ex = np.compress(to_keep, f_ex)
assert specs.shape == (f_ex.size, t2.size, f_em.size)

logging.info("Saving to: {}".format(out_f))
np.savez_compressed(out_f, f_ex=f_ex, t2=t2, f_em=f_em, spec=specs)
del specs
os.remove(buf_fn)
