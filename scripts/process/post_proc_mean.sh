#!/bin/bash

trial=${PWD##*/}  # This is parent folder name, eg: trial_05
data_stack_name=${trial}_data_mean.npz
spec_stack_name=${trial}_spec_mean.npz
# average sequences, data and stab waves for each spectrum
spectra="spectrum*" # pattern matching all spectrum directories
for spec in $(ls -d ${spectra})
do
    # spec is `spectrum_0` without the trailing dash
    for seq_rep in $(ls -d ${spec}/sequence_rep_*.npy)
    # seq_rep is 'spectrum_***\sequence_rep_***.npy
    do
    #Norm in each wfpack. Outputs to "spectrum_*/sequence_rep_*.npy_data_norm.txt"
        python wfpack_avg_norm.py settings.cfg ${seq_rep}
    done
    python seq_avg.py "${spec}/sequence_rep_*_data_norm.txt" "${spec}/timestamps/*" -o ${spec}/data_mean_norm.txt
    python seq_avg.py "${spec}/sequence_rep*.npy" "${spec}/timestamps/*"
    python wfpack_avg.py -o ${spec}/data_mean.txt settings.cfg ${spec}/sequence_mean.npy
    python stabwaves_avg.py -o ${spec}/stab_mean.txt settings.cfg ${spec}/sequence_mean.npy
done
# create volumetric stack for data
python vol_stack.py -o ${data_stack_name} settings.cfg "*/data_mean.txt"
# add stab waves and normed waves to the volumetric stack
python add_stab.py ${data_stack_name} "*/stab_mean.txt"
python add_norm.py ${data_stack_name} "*/data_mean_norm.txt"
# apply 4step phase cycling.
# Other scripts are required for other phase cycling schemes
python cycle4step.py ${data_stack_name}
# Compute 2d spectra stack (weee)
python coh2spec.py ${data_stack_name} ${spec_stack_name}
# Apply rotating frame and clip values of E1 beyond spectrometer range.
# python shift_clip.py -m 670 0.1 ${spec_stack_name}
python shift_clip.py -s settings.cfg ${spec_stack_name}
