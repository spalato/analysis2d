How to run scripts on the server ?
==================================

Running calculations directly on the server can be a big time saver. Performing
data reduction operations (read: averages) directly on the server will save you
a lot of download/upload time. Using the server for calculations requires some 
initial configuration and a bit of know-how. The purpose of this document is 
to get people setup and able to use the standard processing tools.

The synology NAS itself is not a powerful computer, it will not be fast to run 
intensive calculations. It's CPU and RAM are limited since they are normally
only used to manage the internet connections and data handling task. The time
gains come from saving on data downloading and uploading.

The 2D experiments generate 100s GB of data, which are then averaged to a much
smaller size. Performing the averages on your laptop typically requires a few
hours to download the dataset, then perform the averages for minutes, then a
few minutes to upload the averages back onto the server (be nice, be a team 
player). The download time can be cut by performing the averaging process
directly on the server.

The documents starts with some background information, then proceeds to guide
you through the setup process. You should read until [Transferring the data](#transferring-the-data)
(inclusive).
Finally, some details on bash are provided. As always, google is your friend.


Spectra in the shell
====================

The server is a Synology NAS with the DSM operating system. DSM provides a lot
of useful features, including a web-based interface, various connections
protocols etc. All very useful stuff. DSM is a specialized Linux distribution,
and if you go under the hood you should be able to use linux directly. The
linux command line, called the `shell`, is what you will use to run the
calculations. The DSM uses `bash` for a shell. Think of it as a programming
language which is used to control the computer directly, copying files,
launching programs etc.

The shell can be accessed remotely using an SSH connection and client. SSH
is an protocol for encrypted communications which lets you log into a computer
remotely. Once logged in, you'll be in the bash shell. A good client for 
windows is PuTTY.
It seems a bit old-school, but is actually up to date and nothing beats it. 

Bash will let you do everything an operating system normally lets you do: 
navigate folders; read, write, copy, move, delete files; launch programs; etc.
The command line is *very* powerful. Usually, the only limitation of these
shells comes from their lack of graphical interface: they won't show you
windows and buttons, and getting programs to generate images can be a bit hard.
Using the shell effectively is key to realize large time gains. The most common
commands will be presented in section related to bash below.

The averages are performed using bash scripts which call either bash functions
or python scripts. The bash calls are used to find, move and rename files.
The python scripts usually perform data processing operations. In order for the
process to go smoothly, bash needs to be able to find your python scripts
easily and python needs to find your libraries. There is some configuration
required initially (ie: once per user).

After all this setup is done, you can log on to the server, navigate it and
launch python scripts in a batch. This will average the spectra. Note that
the processing scripts will usually require minor tunes to work properly.


Setting up PuTTY
================

This is for Windows users. For non-windows users: your OSX or Linux 
distribution should already come with an SSH installation (open your terminal
and type `ssh`, if you're lucky it comes with some help).

First step is to download and install PuTTY ([putty homepage]). Once that is 
done, launch PuTTY (not puttygen). The PuTTY configuration window will open up.
On the first usage, you need to type in the connection info. You should also 
make a few changes to configuration parameters for better convenience.

- `Connection > Seconds between keepalives`: 30
- `Session > Host Name`: 132.206.175.84
- `Session > Port`: 33686

Under 'Saved sessions', type a session name (PKD2 works fine), and hit 'Save'.
Next time you can just double-click the PKD2 name in the box.

If, for some reason, the server's static IP adress or SSH port number has
changed, you'll have to enter the new parameters. 

- To find the IP adress, browse to the server's quickconnect adress
  (`pkd2.quickconnect.to`). The adress will change to some number like: 
  `132.206.175.84:63257`. The IP adress is everything before the colon.
- To find the port number, log into the server using the web-based interface.
  Go to `Control Panel > Terminal & SNMP > Terminal`. The checkbox "Enable
  SSH service" should be checked. The port number will be written in a box 
  below.
 
Once the configurationof putty is completed, click `Open`. A new window will
open, and the server will greet you with a cold, white `login as:` on black
background. Enter your username. It will then ask for your password. It is
normal that no characters show up when you type in your password. Don't panic!
Just enter your password normally, the keystrokes are still registering.

The prompt will change to:
```
UserName@PKD2:~$
```
Congratulations, you're in!


Configuring the bash environment
=================================

The bash shell works by typing commands. The most necessary commands are:

- To get help, type `help` followed by the command name (`help pwd`), or
  command name followed by `-h` or `--help`. Google is your friend.
- `pwd`: where you are now;
- `ls`: list directory contents;
- `cd <target>`: change directory;
- `cp <source> <destination>`: copy files.

The Synology NAS contains a minimal set of tools, you won't have everything.

Arguments usually can be expanded using wildcards: `sequence_rep*.npy` will
match `sequence_rep_000.npy`, `sequence_rep_001.npy`,
 `sequence_rep_anything_can_go_here_really_but_no_spaces.npy`, 
 `sequence_rep_spaces_need_to_be_escaped_like\ this.npy`, 
 `sequence_rep_seriously_dont_use_spaces.npy`.
Using this effectively requires some practice and googling. See the bash 
section below.

In order to do the calculations, the processing scripts need to find our
libraries. This requires setting the environment variable `PYTHONPATH`. 
There is a folder dedicated to python modules for the group: `/volume1/group/src/pylib/`.
This folder has to be added to the `PYTHONPATH` for every user. 

It's shared in order to centralize the workflow: updates have to be done only
once for everyone. Keep this thing clean and tidy, and consult with your 
labmates when doing changes in there. `git` is more fun alone.

Environment variables need to be defined in a file called `~/.bashrc`.
You know what? Just have mine:
```bash
cd ~                       # go to home directory
cp ~/../SPalato/.bashrc .  # copy
cp -rH ~/../SPalato/bin .  # while we're here, lets' also do this.
```
In order for this to take effect, you may need to start a new SSH session.
Note it also contains other things, related to 
[environment variables](#executing-things-painlessly---environment-variables.)
and [pip](#nothing-works!---installing-from-scratch) installation.


Processing the data
===================
 The data is located in a shared data folder for the group. Due to the structure
of the NAS (many drives, handled properly), the path to the data is not 
straighforward. You can symplify your life by creating a symbolic link to it.
To do this, type: 
```bash
ln -s /volume1/group/data/
``` 
from your home folder. 
You will now have a nice folder in your home called `data`. You can `cd data`,
which should take you to the group data folder.

The data processing scripts should be copied over to the folders you are going
to analyze. This makes sure the specific version of the data processing code
used stays with the data. Steps in the processing are carried out by specific
python scripts (`.py` files), which usually end up calling functions from the
`analysis2d.utils` module. Launching each step independently is tedious: batch
processing is done using `bash` scripts (`.sh` files). These scripts run bash
commands to find files and run python scripts on them. There are two bash
scripts: one to process the averages (called `post_proc_mean.sh`), and one 
to process specific repeats (`post_proc_rep.sh`).
Both will create files like `trial_XXX_data_mean.npz` and 
`trial_XXX_spec_mean.npz`

In order to analyze a run:

1. Navigate to a 2D data folder (by date). 
   For example: `cd ~/data/18-02-23/trial_5`
2. Copy scripts over. I've added an environment variable called `SCRIPTS`
   for all users. `cp $SCRIPTS/* .` 
3. (Optional) Edit the files to your specific needs. This is mostly necessary
   for specific reps. See [here](#editing-files).
4. Launch the batch processing script: `bash post_proc_mean.sh` for averages
   or `bash post_proc_rep.sh` for specific files.
5. Wait... (averages can be long!)
6. If everything went smoothly, you should have two `.npz` files, one with the
   coherences (t1, t2, lambda3), another with spectra (e1, t2, e3). 

If the connection is unstable, run 
`nohup bash post_proc_mean.sh > post_proc_mean.out` instead.
For a breakdown of this call see the bash section [below](#bash).


Editing files
=============
What to change and why
----------------------
All the bash script files are at your disposal, so don't hesitate to hack 
things as required to achieve your goals. By studying the codes, you
should be able to understand what is going on.

Lines from both `post_proc_mean.sh` and `post_proc_rep.sh` need to be changed.
Most are in common, so `post_proc_mean.sh` is broken down first. Note that
some python scripts perform sanity checks using `assert` commands. If you want
to force the script to complete anyways, use the opimization python flag:
`python -O <script.py> <options...> <args...>` 

Here it is:


Setup output file names. You can change this freely to change the name of output
files.
```bash
trial=${PWD##*/}  # Grab current folder name, ex 'trial_05'
data_stack_name=${trial}_data_mean.npz
spec_stack_name=${trial}_spec_mean.npz
```
Loop over all `spectrum_XXX` folders, performing averages, extracting data and
stab waves. You shouldn't need to change anything here.
```bash
# average sequences, data and stab waves for each spectrum
spectra="spectrum*" # pattern matching all spectrum directories
for spec in $(ls -d ${spectra})
do
    # spec is `spectrum_0` without the trailing dash
    python seq_avg.py "${spec}/sequence_rep*.npy" "${spec}/timestamps/*"
    python wfpack_avg.py -o ${spec}/data_mean.txt settings.cfg ${spec}/sequence_mean.npy
    python stabwaves_avg.py -o ${spec}/stab_mean.txt settings.cfg ${spec}/sequence_mean.npy
done
```
Create a stack of data waves, and add the stab waves. Usually, no changes
here either.
```bash
# create volumetric stack for data
python vol_stack.py -o ${data_stack_name} settings.cfg "*/data_mean.txt"
# add stab waves to the volumetric stack
python add_stab.py ${data_stack_name} "*/stab_mean.txt"
```
Apply phase cycling. You need to change this according to you phase cycling
scheme. Ideally, the phase cycling scheme should be selected in the data
acquisition GUI, and saved in the `settings.cfg` file, but it's not done yet.
```bash
# apply 4step phase cycling.
# Other scripts are required for other phase cycling schemes
python cycle4step.py ${data_stack_name}
```
Compute the 2D spectra from the data stack. Note you're likely to want to
customize this step, adding apodization, tuning normalization, etc.
```bash
# Compute 2d spectra stack (weee)
python coh2spec.py ${data_stack_name} ${spec_stack_name}
# Apply rotating frame and clip values of E1 beyond spectrometer range.
```
As of now (18-03-05), the frame shift stored in the settings file is buggy. You
need to manually supply the central wavelength and user frame shift. It's likely
you need to change this. Once that bug is fixed, you can use the settings file
directly (comment first line, uncomment second.) 
```bash
python shift_clip.py -m 670 0.1 ${spec_stack_name}  # manual
# python shift_clip.py -s settings.cfg ${spec_stack_name}  # settings.cfg
```

The processing for specific repeats is mostly identical. The main difference is
you need to specify which `sequence_rep_ZZZ.npy` are processed. The script is
thus a big `for`, with the variable `n` being used to select the reps. Just
list the number of the repeats you want to process. 
```bash
trial=${PWD##*/}  # This is parent folder name, eg: trial_05
for n in 0 2   # Process reps 0 and 2.
do
    rep=$(printf "rep_%03d" $n)  # Pad to 3 digits: 001
    # similar to post_proc_mean.sh. Change as above
    # ...
done
```
Note you can use bash commands: `for n in $(seq 1 5)` will process reps 
1, 2, 3, 4, 5.

How to edit
----------- 
There are three main ways to edit the files. 

You can edit them directly from the shell using `vim`. 
This one does require some training to get used to, but is the most
straightforward as it doesn't require you to open anything else. I suggest you 
look up guides and tutorials. Google is your friend. This is also the *real 
programmer* (TM) way of doing things (almost).  

You can edit the files using the builtin text editor of the NAS web interface.
Open up the server using your browser, open `File Station`, navigate to the
script, right click, `Open with Text Editor`. This should also be
straightforward.

Finally, for more complex edits you may want to use your favorite text editor 
on your own computer. Just transfer the script back and forth until you're 
happy (using git, total commander or whatever). The idea is seductive, but
you'll quickly get tired of the hassle, believe me. 


Transferring the data
=====================
Use [Total Commander]. I won't cover it's use here, just ask around.


Nothing works! - Installing from scratch
============================
The data server has a great habit of wiping the default location for python
libraries when we update it. In order to avoid this, a custom location for
python modules has to be designated. Currently, this is: 
`/volume1/group/src/pylib/`. All that is needed for installation to go smoothly
is the configuration of two environment variables: `PYTHONPATH` and
`PIP_TARGET`. Both need to be set to the selected folder by adding the
following lines to `~/.bashrc`. Both should be there if you copied my `.bashrc`
file:
```bash
lib_path=/volume1/group/src/pylib/
export PYTHONPATH=$lib_path
export PIP_TARGET=$lib_path
```
The role of these specific environment variables is covered [below](#executing-things-painlessly---enviromnment-variables.).

Updating the server also wipes `pip` out, which you need to get back. You can
get it using a script at `/volume1/group/src/get_pip.py`.

If it's lost, you can download the file using:
```bash
wget https://bootstrap.pypa.io/get-pip.py
```

In any case, go to where the file is located, and run `sudo python get_pip.py`.
You can then `pip install` all the modules you need (such as `fuckit.py`).

The recommended way to install modules is in *editable mode*. This creates a
link from the python library to wherever you module sits. Changes to the files
in the module will be picked up when launching the python interpreter. 
Normally, you can simply navigate to the package's `setup.py` file and run:
```bash
pip install -e .
```

Sadly, as of now (18-03-05), there is a bug in `pip` that prevents using the 
`editable` and `target` options simultaneously (URGH). The fix is [almost in](https://github.com/pypa/pip/pull/4557),
in the meantime you have to:

1. Navigate to the project root, where the `setup.py` and `requirements.txt` 
 files are located;
2. `pip install -f requirements.txt`
3. Creata a symlink in custom python module folder: `ln -s $PWD/src/analysis2d $PIP_TARGET`

Cheers. Note this is will not be required once the bugfix is in.

Note that skipping updates is a *very* bad idea! Synology NAS have been targeted
by ransomwares in the past. Keep those hacky Uzbeks away from your precious
data!

Bash
=====

Bash provides a mature, powerful set of tools for adminstrative tasks. The most
common tools and concepts are presented below. A few helpful tricks are then 
collected in a cheatsheet. Google is your friend.

Basics
------

The most basic navigation and administrative commands are listed below. Of
course, all these functions have advanced usages and can take many arguments.
Some salient cases are discussed in the next section.

```bash
pwd          # this is a comment. pwd will show you the current directory.
echo <thing> # print content/output of thing to terminal.
cat <files>  # Concatenate contents of files. Print to terminal.
ls <target>  # list directory contents. Defaults to current ('.')
ls *txt      # '*' is a wildcard. This will list anything ending in 'txt'.
cd <target>  # change to directory. Defaults to home ('~')
cp <source> <target>  # copy source to target
mv <source> <target>  # move source to target (used for renaming)
rm <target>           # delete target
ln -s <source>        # make symbolic link to target
unlink <target>       # remove link
mkdir <name>          # create directory
rmdir <target>        # delete empty directory
printf <format> <n>   # format string syntax (ie: 
grep <regex> <target> # find occurances of regular expression regex in target
awk <regex> <target>  # similar go grep, sometimes more powerful
find          # find stuff (many options!)
bash <script> # launch bash script
<command> > <target>  # write output of command to target file, `pwd > path.txt`
<command> >> <target> # append output of command to target file, `pwd >> path.txt`
<command> &   # execute command in another process (forking)
ps            # list owned processes (including things created by forking)
jobs          # list spawned jobs (those created by forking)
nohup <command> # don't kill process on disconnect
<cmd1> | <cmd2> # 'pipe' the output of cmd1 to cmd2
$<variable>     # use the value stored in variable
${variable}     # same as above, more flexible
$(<command>)    # use the output of command as if stored in a variable
du              # disk usage (for folders)
chmod <change> <file>  # change permissions for file (ie: read, write, execute)
dirs            # list directory stack (see below)
pushd <target>  # add current directory on the stack, than cd to <target>
popd            # remove current directory from the stack, than cd to next one
```

Executing things painlessly - Environment variables.
----------------------------
Three environment variables can be used to maximize your enjoyment (or minimize
the pain) in using the server. Environment variables can be used for any
purpose really (you can create your own for convenience, for example), but a
lot of applications are configured though them. 

Environment variables cacan be set many ways, but here we'll focus on user environment
variables defined in the `~/.bashrc` file. This file is simply a bash script
that is run whenever a session logs in. Any variables `export`ed will be
available in the current session. As such, a change made to the bashrc script
is not effetive immediately. You need to run it (`bash ~/.bashrc`) or start a
new session.

There are four main environment variables that we need to discuss: 
1. `PATH` is used by bash to search for commands. **DON'T OVERWRITE!**, append
    instead;
2. `PYTHONPATH` is used by python (and pip) to search for modules;
3. `PIP_TARGET` is used by pip to install modules;
4. `SCRIPTS` is used as a shortcut.

Both `PATH` and `PYTHONPATH` can contain multiple enties, separated by `:`.
Their respective programs will search that list in order until they find the
required item.

You can add a folder to `PATH` for specific commands you want to execute from
anywhere. I suggest `~/bin/` (`~` is your home folder). You can
create this folder and fill it with all the nice exectuables you want.
To extend this variable, you need the following line in `~/.bashrc`:
```bash
export PATH=$PATH:~/bin   # append ~/bin to PATH
```

Note that unlike windows, any file can be executable. To execute scripts, they
need to start with a "shebang". The first line will indicate the proper
interpreter  for the script like this:

- `#!/bin/python` for python
- `#!/bin/bash` for bash

To make a file executable, type `chmod u+x <file>`. Note this can be a
symbolic link! This way, you don't have to copy the file in your `bin` folder
every time you change it.

If you followed the configuration instructions [above](#configuring-the-bash-environement)
you should already have a that folder setup with a few links in there. To check
this, move anywhere and type `npz_shape`. It should complain about missing
arguments. Hurray!

The `PYTHONPATH` environment variable is needed by python to find modules
installed in non-default locations. As mentionned earlier, this is required in this case.  

The `PIP_TARGET` environment variable lets `pip` know where to install current
modules. This sets the `--target` command line switch. Note that currently
(18-03-05), this clashes with the `--editable` switch.

The `SCRIPTS` environment variable is for convenience. It points to the
`analysis2d/scripts/processing` directory. 

Cheatsheet
----------

**Shortcuts**:
```bash
~    # Home directory.  `ls ~` will show the contents of your home directory
.    # Current directory. File with names that start 
     # with '.' (such as `.bashrc`) are invisible files. See `ls -a`
..   # Parent directory. `ls ~/..` should list all Home folders.
```

**Navigating**:
```bash
cd <target>  # change directory. Target is a relative path, unless it starts
             # with '/'
cd ~/bin     # `~` is a shortcut for your home directory.
cd           # go home, same as `cd ~`
cd ..        # move to parent directory
pushd <target> # move somewhere
popd         # go back where you were when typed pushd
```

**Listing things**:
```bash
ls      # list content of current directory
ls -d * # list current subdirectories
ll      # same thing, long format. Note: alias for `ls -l`
ll -h   # long format ls, display file size in human-readable format.
ls -a   # list all content, including invisble files.
```

**Using patterns**: Keep your directory trees well organized and patterns 
become very powerful. It's powerful, so also be careful! Sometimes a single 
missing character will make you delete wayyy too many things.
Assuming you're in a date folder, containing 2D data (ex: data/2de/17-07-19):
```bash
ls 2D/trial5/*_data_mean.txt # find all data_mean.txt files in trial5
ls -d 2D/trial5/spectrum*/   # find all spectrum_XX directories in trial5
ls -d 2D/trial*/spectrum*/   # find all spectrum_XX dirs in any trials
cp */*/*_data_mean.txt <dest> # copy all data_mean.txt to destination folder
rm 2D/trial5/*/sequence_mean.npy # DELETE all sequence_mean.npy files in trial5
```
The last line should make it obvious how things can go wrong.
For reference, see [Bash parameter substitution] and [Bash pattern matching].

**pipes**: Pipes let you use the output of a command as the input to another.
A common usage pattern is to filter elements.
```bash
ps aux | grep SPalato # list all processes, keep the lines where SPalato
                      # shows up.
ls <pattern> | grep <filter>  # grep is more powerful than wildcards
```

**using nohup**: You want to launch a long calculation, than go home have
 beers? You need to use `nohup`. Normally, if you are disconnected when 
running a long task, the server will kill the task. This will happen even if 
you fork the task to a child process. Using `nohup` will prevent this. 
However, the process can't redirect it's output to the terminal anymore (it may
go missing). By default, `nohup` will redirect to a file called `nohup.out`.
This should be changed using the redirect operator `>`. Finally, there is no
reason not to fork anymore, so the fork operator comes last. This result in
calls such as:
```bash
nohup bash run_seq_avg.sh > seq_avg.out &
```

**regexes**: Regular expressions (regex) are used to do complex search queries.
They are used by tools such as `grep`, `egrep`, `awk`, `sed`, and can be used
in `notepad++`, `total commander` and have modules for most programming 
languages. They are rather powerful, but rather complex and require practice.
You should have a look at the [wikipedia regex page], the [notepad++ help] 
page and the [python `re` module] page. To get some practice, have a look at
[regex golf].



References
==========

- putty homepage: https://www.chiark.greenend.org.uk/~sgtatham/putty/
- Bash parameter substitution: https://www.tldp.org/LDP/abs/html/parameter-substitution.html
- Bash pattern matching: http://wiki.bash-hackers.org/syntax/pattern
- wikipedia regex page: https://en.wikipedia.org/wiki/Regular_expression
- notepad++ help: http://docs.notepad-plus-plus.org/index.php/Regular_Expressions
- python `re` module: https://docs.python.org/2/library/re.html
- regex golf: https://alf.nu/RegexGolf
- Total Commander: https://www.ghisler.com/

[putty homepage]: https://www.chiark.greenend.org.uk/~sgtatham/putty/
[Bash parameter expansion]: http://wiki.bash-hackers.org/syntax/pe
[Bash pattern matching]: http://wiki.bash-hackers.org/syntax/pattern
[wikipedia regex page]: https://en.wikipedia.org/wiki/Regular_expression
[notepad++ help]: http://docs.notepad-plus-plus.org/index.php/Regular_Expressions
[python `re` module]: https://docs.python.org/2/library/re.html
[regex golf]: https://alf.nu/RegexGolf
[Total Commander]: https://www.ghisler.com/