Analysis2D
==========

Data analysis tools for multidimensional spectroscopy in the PK lab at McGill.
Designed for python2.7 (cuz that's how we rolled), but mostly works for python3.
Porting shouldn't really be an issue.

Overview
========
This project if for the data analysis of 2D spectroscopy in the PK labs. It
mainly consists for a python package (``analysis2d``) and associated scripts.

It also contains documentation that you should **READ** (most importantly:
[HOWTO_NAS.md])

Installation
============

Recommended installation using git and pip:

```bash
git clone https://bitbucket.org/spalato/analysis2d.git
cd analysis2d
pip install -e .
```

This clones the latest version locally, and installs the module in editable
mode.

Installation on the data server is a bit more complicated, see [HOWTO_NAS.md],
section titled 'Nothing Works - Installing from scratch'. The current working
version is in `group/src/analysis2d/`. Anyone should be able to run `git pull
origin master` from there.

Contents
========
Documentation
-------------

- [`HOWTO_NAS.md`](HOWTO_NAS.md) Why and how to process data on the network assisted storage (NAS).
  You definitely should read this one!
- [`tools.md`](tools.md) Description of the tools in `scripts/tools/`.


Package
-------
The python package is contained in `src/analysis2d/`. It mostly contains an
unfinished GUI project. Usable module files are:

- `globfit.py` Global analysis tools.
- `plot_utils` Tools for quickly plotting 2D data.
- `utils.py` Where most stuff ended up.

Refer to the documentation from the modules and functions for more information.

Scripts
-------
The `/scripts/process/` folder contains scripts used by the main data processing
pipeline. See [HOWTO_NAS.md] for details on how to run everything. Read the 
scripts help (`python <script.py> --help`) and code for more information.

Tools
-----
The `/scripts/tools` folder contains scripts that are not part of the main
data processing pipeline. See [tools.md].

- `npz_shape.py` List content of `.npz` archive.
- `staturation.py` Prepare a report on saturated pixels.
- `sorted_spectra.py` List the spectra folder in numerical order.
- `summarize.py` Summarize trials. Very useful!

License
=======
This is a project internal to the PK lab, it's of little use for anyone else...

TL;DR
=====
Read [HOWTO_NAS.md] first. (then everything else.)

[HOWTO_NAS.md]:HOWTO_NAS.md
[tools.md]:tools.md