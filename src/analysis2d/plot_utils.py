#!/bin/python
# utils.py: 2D plotting utilities

from __future__ import division, print_function
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def _expand_axis(x):
    """Expand axis by first value, for use by pcolormesh"""
    new = np.zeros(x.shape)
    new[1:] = 0.5*(x[1:]+x[:-1])
    first = 2*new[1]-new[2]
    new[0] = first
    return new

_default = object()

def plot_2d(x, y, z,
            cmap="seismic", levels=_default, colors="0.9",
            symz=True, diag=True,
            pcolor_kw=_default, contour_kw=_default):
    """
    Plot a 2d map. Thin wrapper over pcolormesh and contour.

    This also fixes poorly documented behavior of pcolormesh by which it clips
    the last rows/columns if z has same number of points as x, y. If needed,
    x, y will be extended by an extra point such that all z values are
    represented. The extra x and y value uses the spacing of the last two
    values.

    Parameters
    ----------
    x : (N,) np.ndarray
        x values. If of shape (N+1,), won't extend.
    y : (M,) np.ndarray
        y values. If of shape (N+1,), won't extend.
    z : (M,N) np.ndarray
        z values
    cmap : matplotlib colormap
        Colormap for pcolormesh, defaults to seismic
    levels : matplotlib levels, (iterable, integer)
        Contour levels
    colors : matplotlib colors
        Contour color
    symz : bool or float
        Use a symmetric colormap. If True, uses max absolute value as a limit
        in both directions. If a scalar, will use it as a limit in both
        directions, overriding vmin and vmax keywords.
    diag : bool or matplotlib color
        Show dotted diagonal. Defaut color is "0.2".

    Extra kwargs get passed down to pcolormesh and contour.

    Returns
    -------
    quads : QuadMesh
    contour : QuadContourSet
    """
    assert x.ndim==1
    assert y.ndim==1
    assert z.ndim==2
    if x.size == z.shape[1]:
        x_pix = _expand_axis(x)
    else:
        x_pix = x
        x = x[:-1]
    if y.size == z.shape[0]:
        y_pix = _expand_axis(y)
    else:
        y_pix = y
        y = y[:-1]
    pcolor_kw = {} if pcolor_kw is _default else pcolor_kw
    contour_kw = {} if contour_kw is _default else contour_kw
    pcolor_kw["cmap"] = cmap
    lim = np.max(np.abs(z))
    if type(symz) == bool:
        if symz:
            for k, s in zip(["vmin", "vmax"], (-1, 1)):
                if k not in pcolor_kw:
                    pcolor_kw[k] = s*lim
    else:
        pcolor_kw["vmin"] = -symz
        pcolor_kw["vmax"] = symz
    if levels is _default:
        levels = np.linspace(-lim, lim, 21)
    elif type(levels) == int:
        if symz:
            levels = np.linspace(-lim, lim, levels)
    contour_kw['levels'] = levels
    contour_kw['colors'] = colors
    quad = plt.pcolormesh(x_pix, y_pix, z, **pcolor_kw)
    contour = plt.contour(x, y, z, **contour_kw)
    if diag:
        if type(diag) == bool:
            diag = "0.2"
        l = np.min([np.min(v) for v in (x, y)])
        u = np.max([np.max(v) for v in (x, y)])
        diag = plt.plot([l,u], [l,u], ":", c=diag)
    return quad, contour, diag
