__author__ = 'Helene_Seiler'
import numpy as np
import cmath
from pyqtgraph.Qt import QtCore, QtGui
import pyqt_image_analysis as plot_hub
from glob import glob

class PhaseCycling(object):
    def __init__(self, steps=[1, 1], coeff=[1, 1]):
        super(PhaseCycling, self).__init__()
        # self.steps contains the number of steps for each phase (e.g 1 X 1 X 5)

        self.steps = steps
        self.coeff = coeff
        self.phase_diff = np.zeros(len(self.steps))
        for i in range(len(self.phase_diff)):
            self.phase_diff[i] = 2 * np.pi / self.steps[i]

    """
    Method undo_phasecycling: raw_data needs to be a list
    """

    def undo_3pulses_phasecycling(self, raw_data):

        assert len(self.steps) is 2

        L = self.steps[0]
        M = self.steps[1]
        coh_time_no = int(len(raw_data[:, 1]) / L * M)
        phase_cycled_signal = np.zeros((coh_time_no, len(raw_data[1, :])))

        for i in range(coh_time_no):

            for m in range(M):
                for l in range(L):
                    #print l
                    #print self.phase_diff[0]
                    #print np.exp(-1j * self.coeff[0] * self.phase_diff[0] * l)

                    phase_cycled_signal[i, :] = np.add(phase_cycled_signal[i, :],
                                                       (np.exp(-1j * self.coeff[0] * self.phase_diff[0] * l)*
                                                        np.exp(-np.imag(1) * self.coeff[1] * self.phase_diff[
                                                            1] * m)) * raw_data[i * L * M + m * L + l, :])
                    # Divide by L times M?
                    #phasecycled_signal.append(temp)

        return phase_cycled_signal
        
    def do_3pulses_phasecycling(self):
        
        L= self.steps[0]
        M=self.steps[1]
        
        Delta_phi12= 2*np.pi()/L
        Delta_phi13= 2*np.pi()/M
        
        phase12=[]
    
        for l in range(L-1):
            phase12= l* Delta_phi12
            
        for m in range(M-1):
            phase13= m*Delta_phi13
            
        return Delta_phi12, Delta_phi13


if __name__ == '__main__':

    import sys
    import os
    #if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
    #    QtGui.QApplication.instance().exec_()
    app = QtGui.QApplication(sys.argv)
    #f = np.loadtxt('test.txt', delimiter=',')
    f=open('2D_1.csv')
    lines=f.readlines()
    wavelengths= lines[1].split(',')
    del wavelengths[0]
    del wavelengths[0]
    wavelengths = map(float, wavelengths)
    np.savetxt('wavelengths_acton.txt', wavelengths)
    #print wavelengths
    mean_files=[]


    for i in glob("Y:/GROUP/experimental/data/2D/080814/2D/RUN5/*csv") :
        print i
        mean_files.append(np.loadtxt(i, delimiter=',',skiprows=3))

    #print os.listdir("Y:/GROUP/experimental/data/2D/080814/2D/RUN5")
    #for i in os.listdir("Y:/GROUP/experimental/data/2D/080814/2D/RUN5"):
    #    if i.endswith(".csv"):
    #        print "Are we going there"
    #        print i
    #        mean_files.append(np.loadtxt(i, delimiter=',',skiprows=3))
    #    else:
    #        pass

    print np.shape(np.array(mean_files))
    mean_array=np.mean(np.array(mean_files),axis=0)
    stdev_array=np.std(np.array(mean_files), axis=0)
    np.savetxt('mean_array.txt', mean_array)
    np.savetxt('std_array.txt', stdev_array)


    #l = []
    #l = [ line.split() for line in f]

    #l = [ map(int,line.split(',')) for line in f ]
    #l = [ map(int,line.split(',')) for line in f if line.strip() != "" ]
    #print len(l)
    #print (l[4])

    test = PhaseCycling(steps=[2, 1], coeff=[-1, 1])
    phase_cycled_signal = test.undo_3pulses_phasecycling(mean_array)
    print np.shape(phase_cycled_signal)
    #np_array= np.array(phase_cycled_signa,l)
    #print np.shape(np_array)

    plot_hub=plot_hub.PyQtImageWidget()
    plot_hub.plot_data(phase_cycled_signal)
    sys.exit(app.exec_())