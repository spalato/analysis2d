#!python3
# utils.py: 2D analysis utilities

"""
2danalysis.utils: common 2D analysis tools.

Requires:
---------
numpy
tqdm
"""

from __future__ import division, print_function
import warnings
import os.path as pth
import re
import tqdm
import numpy as np
from numpy.fft import fft, fftfreq, fftshift
from itertools import compress
import logging
try:
    from ConfigParser import ConfigParser
except ImportError:
    from configparser import ConfigParser
logger = logging.getLogger(__name__)

def nm2eV(arr):
    """
    For transforming x axis from wvl to eV
    """             
    return 6.626*2.998e2/(1.602*arr)

class ConfigLoader(object):
    """
    Handles loading and saving of configuration dict to INI-style configfile.

    Reads a single section. Know type information. Paths are taken relative to
    the config file's directory.
    """
    str_fields = ['experiment_type', 'comment', 'path_data']
    path_fields = ['wavelengths', 'folders']
    int_fields = ['n_pulses', 'n_exp', 'n_rep']
    float_fields = ['delta_t1', 'delta_t2', 't1_ini', 't1_final', 't2_ini',
                    't2_final',
                    'phi_ini', 'phi_final', 'delta_phi', 'alpha_ini',
                    'alpha_final', 'delta_alpha',
                    'pump_power', 'probe_power', 'pulse1_power', 'pulse2_power',
                    'rot_frame_freq']
    listfloat_fields = ['t2', 'pulse1_phases', 'pulse2_phases']

    def load(self, fobj, section):
        """
        Load a config dict from key-value pairs in file-like object fobj.

        Values are in INI-style configfile. Values in 'section'
        are loaded using python's configparser.
        """
        root = pth.abspath(pth.dirname(fobj.name))
        parser = ConfigParser()
        parser.readfp(fobj)
        config = {}
        for k, val in parser.items(section):
            k = k.lower()
            if k in self.str_fields:
                config[k] = str(val)
            elif k in self.path_fields:
                config[k] = self.load_path(val, root)
            elif k in self.int_fields:
                config[k] = self.load_int(val)
            elif k in self.float_fields:
                config[k] = float(val)
            elif k in self.listfloat_fields:
                config[k] = self.load_listoffloat(val)
            else:
                config[k] = val
        return config

    def save(self, config_dict, fobj, section):
        """
        Saves configdict to fojb in INI-sytle configfile, under 'section'
        """
        root = pth.abspath(pth.dirname(fobj.name))
        parser = ConfigParser()
        parser.add_section(section)
        for k, v in sorted(config_dict.items()):
            k = k.lower()
            if k in self.str_fields:
                v = self.fmt_str(v)
            elif k in self.path_fields:
                v = self.fmt_path(v, root)
            elif k in self.int_fields:
                v = self.fmt_int(v)
            elif k in self.float_fields:
                v = self.fmt_float(v)
            elif k in self.listfloat_fields:
                v = self.fmt_listoffloat(v)
            else:
                v = repr(v)
            parser.set(section, k, v)
        parser.write(fobj)

    @staticmethod
    def load_listoffloat(val):
        """Converts a string representing a list of int to actual object"""
        return [float(v) for v in val.strip(' []').split(', ')]

    @staticmethod
    def fmt_listoffloat(val):
        """Formats a list of int for output"""
        return str(val)

    @staticmethod
    def load_listofstr(val):
        """Converts a string representing a list of str to actual object"""
        return val.strip(' []').split(', ')

    @staticmethod
    def fmt_str(val):
        """Formats a list of string for output"""
        return str(val)

    @staticmethod
    def load_path(val, root=''):
        """Loads a path. Prepends 'root' to the path"""
        return pth.abspath(pth.join(root, val))

    @staticmethod
    def fmt_path(val, root=''):
        """Formats a path for output. Returns path relative to 'root'"""
        return pth.normpath(pth.relpath(val, root))

    @staticmethod
    def load_int(val):
        """Converts a string representing an int to actual object"""
        return int(val)

    @staticmethod
    def fmt_int(val):
        return int(val)

    @staticmethod
    def fmt_float(val):
        return float(val)


def valid_timestamps(list_of_ts, acq_time=0.001):
    """
    Validate repetitions using timestamps.

    Parameters
    ----------
    list_of_ts: iterable containing the timestamps filenames.
    acq_time: CCD acquisition time, to set a threshold for bad shots (s)

    Returns
    -------
    shot_valid: list containing booleans. True means the repetition is good to keep in the average.
    """
    shot_valid = []
    for idx, ts_fn in enumerate(list_of_ts):
        #try:
        #np.seterr(all='raise')
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            ts = np.loadtxt(ts_fn)
        #except Runt
        if not list(ts):
            logger.debug("empty timestamps: {}".format(ts_fn))
            shot_valid.append(False)
        else:
            deltas = np.abs(ts[:-1]-ts[1:])
            valid_timing = np.max(np.abs(deltas-acq_time)) <= 0.5 * acq_time
            if not valid_timing:
                logger.debug("invalid timing: {}".format(ts_fn))
            shot_valid.append(valid_timing) # t[n]-t[n-1]<dt/2
    return shot_valid


def load_sequences(list_of_files, list_of_ts=None, pbar="Loading sequence"):
    """
    Load a list of experiment result files. Does the average as it loads.

    Parameters
    ----------
    list_of_files : iterable of file names to be read.
    list_of_ts : iterable of timestamps file names
    pbar : String
        Prefix for progress bar. If false, don't display progress bar.

    Returns
    -------
    data_mean : (N, M) np.ndarray
        Averaged sequences.  N wl, M spectra.
    """
    n_exp = len(list_of_files)
    n_files = 0
    bad_files = 0
    data_mean = None
    bad_timing = 0
    if list_of_ts:
        logger.debug("Loading timestamps")
        #list_of_ts = [np.loadtxt(f) for f in list_of_ts]
        list_of_valid = valid_timestamps(list_of_ts)
        bad_timing = list_of_valid.count(False)
        list_of_files = list(compress(list_of_files, list_of_valid))

    it = enumerate(list_of_files)
    if pbar:
        it = tqdm.tqdm(it,
                       total=len(list_of_files),
                       desc=pbar)
    for idx, filename in it:
        if pth.getsize(filename) < 100:
            bad_files += 1
            continue
        n_files += 1
        if filename.endswith('.npy'):
            current = np.load(filename)
        else:
            current = np.loadtxt(filename, delimiter=' ')
        if np.max(current) > 65500:
            logger.warning("Saturation detected: {}".format(filename))
        if data_mean is None:
            data_mean = current/n_exp
        else:
            data_mean += current/n_exp
    logger.info("avg: {} - bad files: {} - bad timing: {}".format(n_files, bad_files, bad_timing))
    data_mean *= n_exp/n_files
    return data_mean


def acton_wl(lower=-np.infty, upper=np.infty, retmask=False, fname=None):
    """
    Convenience function to retrieve acton wavelengths.

    Parameters
    ----------
    lower : float
        Lower bound on wavelength.
    upper
    fname
    """
    if fname is None:
        root = pth.dirname(__file__)
        fname = pth.join(root, '160518acton_wavelengths650nm.txt')
    wl = np.loadtxt(fname, delimiter=' ')
    mask = np.logical_and(wl>lower, wl<upper)
    if retmask:
        return wl[mask], mask
    return wl[mask]


def load_exp_config(fname):
    """
    Loads experiment config. Returns params dict, times and n_rep

    Parameters
    ----------
    fname : path to config file.

    Returns
    -------
    params : dict
        Configuration information
    times : np.ndarray(N,)
        Times or scan axis
    n_rep : int
        Number of pattern reps
    """
    with open(fname) as cfg_f:
        params = ConfigLoader().load(cfg_f, "ExpConfig")
    time = np.arange(params['t1_ini'], params['t1_final'],
                     params['delta_t1'])
    n_rep = int(params['n_rep'])
    return params, time, n_rep


def split_complete(data, n_reps, n_waves=38):
    """
    Split data in a complete stack of chunks and the rest.

    Parameters
    ----------
    data : (N, M) np.ndarray
        Averaged data. N is the number of wl, M is the number of spectra
    n_reps : int
        Pattern reps
    n_waves : int
        Number of waves per pack. Defaults to 38
    """
    # TODO: test # seems ok
    n_complete = data.shape[1]//(n_reps*n_waves)
    return np.split(data, [n_complete*n_reps*n_waves], axis=1)


def chunk(data, n_reps, n_waves=38):
    """
    Returns data chunked by packs and reps, or by reps if a single pack.

    Parameters
    ----------
    data : (N, M) np.ndarray
        Data to chunk along axis 1 (last). N is the number of wl, M is the
        number of spectra.
    n_reps : int
        Pattern repetition number.
    n_waves : int
        Number of waves per pack. Default to 38

    """
    # TODO: test, seems ok
    ax = 1
    if data.shape[1] > n_reps*n_waves:
        # contains multiple packs
        n_complete = data.shape[1]//(n_reps*n_waves)
        data = np.array(np.split(data, n_complete, ax))
        ax += 1  # ugly. Kambhampati style
    return np.array(np.split(data, n_reps, axis=ax))


def rem_stab_waves(data, n_stab=2, axis=-1):
    """
    Remove the stab waves from data.

    Removes the first 'n_stab' spectra along 'axis'
    """
    # TODO: test, seems ok.
    if axis < 0:
        axis = data.ndim+axis
    return np.split(data, [n_stab], axis=axis)[1]


def keep_stab_waves(data, n_stab=2, axis=-1):
    """
    selects stab_waves in dataset
    """
    # TODO: test, seems ok.
    if axis < 0:
        axis = data.ndim + axis
    return np.split(data, [n_stab], axis=axis)[0]


def stab_wave_mean(data, n_reps, n_waves=38, n_stab=2):
    """
    Averages the data, keeping only stab waves.

    Parameters
    ----------
    data : (N,M) np.ndarray
        Data. N is size of wl array.
    n_reps : int
        Number of pattern reps
    n_waves : int
        Number of waves per pack. Defaults to 38
    n_stab : int
        Number of stab waves. Defaults to 2

    Returns
    -------
    data : (N,M') np.ndarray.
        Data. N is size of wl array, M is size of time array.

    """
    complete, rest = [keep_stab_waves(chunk(c, n_reps, n_waves).mean(axis=0),
                                            n_stab)
                      for c in
                      split_complete(data, n_reps, n_waves)]

    if complete.ndim == 3:
        complete = np.hstack(complete)
    assert complete.ndim == 2
    assert rest.ndim == 2
    assert complete.shape[0] == rest.shape[0]
    return np.hstack([complete, rest])


def split_chunk_mean(data, n_reps, n_waves=38, n_stab=2):
    """
    Averages the data, stripping stab waves.

    Parameters
    ----------
    data : (N,M) np.ndarray
        Data. N is size of wl array.
    n_reps : int
        Number of pattern reps
    n_waves : int
        Number of waves per pack. Defaults to 38
    n_stab : int
        Number of stab waves. Defaults to 2

    Returns
    -------
    data : (N,M') np.ndarray.
        Data. N is size of wl array, M is size of time array.
    """
    # TODO: test  # seems ok
    complete, rest = [rem_stab_waves(chunk(c, n_reps, n_waves).mean(axis=0),
                                     n_stab)
                      for c in
                      split_complete(data, n_reps, n_waves)]
    if complete.ndim == 3:
        complete = np.hstack(complete)
    assert complete.ndim == 2
    assert rest.ndim == 2
    assert complete.shape[0] == rest.shape[0]
    return np.hstack([complete, rest])

def seqrep2data(data, n_rep, n_waves=38, n_stab=2):
    """
    Takes a sequence rep with stab waves and waveform waves in shape.
    Normalizes waveform waves with the preceding data waves every 'n_waves'.
    This makes it as close to ratiometry as possible without doing pump chopping.
    
    Previously we weer getting a much more washed out stab wave mean and data wave mean,
    so correlated fluctuations in stab waves weren't taken into account.
    This should decrease some high frequency fiber noise.
    
    Parameters
    --------------
    data   : (N,M) np.ndarray
            N is wvl array
            M is total number of spectra
    n_rep  : int,
            Number of sequence_reps.
    n_waves: int,
            Number of waves in a sequence rep
            
    Returns
    ---------------
    (N,M') np.ndarray 
        M' is M with stab waves removed and average over n_rep.
        Waveform normalization every 38 waves, mean over repetitions
    """
	#Background Correction. Only take 98% of background to avoid divide by 0 error.
    bkgrd = 0.49*np.mean(data[:10,:]+data[-10:,:])
    #Splitting data into wfpacks with 38 waves each and those with fewer.
    n_complete = data.shape[1]//(n_rep*n_waves)
    wfpacks = np.split(data,[n_complete*n_rep*n_waves],axis=-1)
    #Normalising each wfpack by mean of 2 stab waves, then by mean over sequence reps
    comp,rest = [rem_stab_waves(chunk(c,n_rep,n_waves)-bkgrd,n_stab)/
	             np.expand_dims(np.mean(keep_stab_waves(chunk(c,n_rep,n_waves)-bkgrd,n_stab),axis=-1),-1) for c in wfpacks]
    comp,rest = [np.mean(c,axis=0) for c in (comp,rest)]
    if comp.ndim == 3:
        comp = np.hstack(comp)
    assert comp.ndim == 2
    assert rest.ndim == 2
    assert comp.shape[0] == rest.shape[0]
    return np.hstack([comp, rest])

def reverse_along(arr, axis):
    """Reverse array along an axis.
    """
    # swap axes to set target axis as first. Reverse along first
    # undo swap axes.
    return np.swapaxes(np.swapaxes(arr, 0, axis)[::-1],
                       0, axis)


def wl2freq(wl, trace, axis=-1):
    """
    Convert trace from wl space to frequency with appropriate jacobian transform.

    Returns frequencies, trace. Not sure that works well for arrays with 1 dim.
    """
    # TODO: make work with ndim=1, 2, ...?
    # TODO: I am unhappy with that transform. There is no real need to jacobian
    #       correct discrete data. We already have the bin spacings.
    c = 3E8 * 1E9 / 1E15  # nm/fs
    freq = c/wl
    jacob = c/(wl**2)
    target_shape = [-1]
    target_shape.insert(1-axis, 1)
    trace = trace / jacob.reshape(target_shape)
    trace = reverse_along(trace, axis)
    return freq[::-1], trace


def cycle(data, scheme=2):
    """
    Cycles data to extract 2d signal.


    Parameters
    ----------
    data : (N,M) np.ndarray
        Data, N wl, M spectra
    scheme : ?
        phase cycling scheme. We still need a good abstraction for that I guess.

    Returns
    -------
    cycled : (N, M') np.ndarray
        Data, N wl, M times
    """
    if scheme != 2:
        raise NotImplementedError("Phase cycling scheme {!s} not implemented".format(scheme))
    return data[:,::2]-data[:,1::2]





def coh2spec(t, wl, coh, zpad=4):
    """
    Convert t-wl coherence to f-f 2D spectrum.

    Fourier transforms along t and converts wl to f. Intensities are corrected
    for un-even wl spacing and for wl-f transformation. Units are assumed to be
    fs, nm.

    Parameters
    ----------
    t : (N,) np.ndarray
        Time values. Constant spacing is assumed.
    wl : (M,) np.ndarray
        Wavelength array
    coh : (M,N) np.ndarray
        Coherence (ie: cycled)
    zpad : int
        Zero-padding factor.

    Returns
    -------
    f_ex : (N',) np.ndarray
        Excitation frequencies. N' is approx zpad*N/2
    f_em : (M,) np.ndarray
        Emission frequencies.
    spec : (M, N') np.ndarray
        2D spectrum. Appropriately corrected for jacobian (the result is a
        distribution function)
    """
    assert t.ndim == 1
    assert wl.ndim == 1
    assert coh.ndim == 2
    c = 3E8 * 1E9 / 1E15  # nm/fs
    nfft = t.size*zpad
    f_ex = fftshift(fftfreq(nfft, t[1]-t[0]))
    coh = coh.copy()
    coh[:,0] *= 0.5
    spec = fftshift(fft(coh, nfft, axis=1), axes=1)
    #mask = f_ex<=0
    #spec = spec[:,mask]
    #f_ex = f_ex[mask]
    f_em = c/wl
	#finite Jacobian transform
    d_f_em = np.zeros(f_em.shape)
    d_f_em[:-1] = f_em[1:] - f_em[:-1]
    d_f_em[-1] = d_f_em[-2]  # repeat last value. Shouln't be meaningful data anyways
    spec /= d_f_em[:,np.newaxis]
    return f_ex, f_em, spec


def superg_apodization(t1, coh, n=2, tau=75):
    window = np.exp(-((t1/tau)**(2*n)))
    target_shape = [1 for i in range(coh.ndim)]
    target_shape[0] = window.size
    return coh*window.reshape(target_shape)


def is_stab_wave(ntot, nrep, n_waves=38, nstab=2):
    n_complete = ntot//(nrep*n_waves)
    complete_len = n_complete*nrep*n_waves
    is_stab_waves = np.full(ntot, False)
    idx = np.arange(ntot)
    is_stab_waves[(idx<complete_len) & (idx % n_waves < nstab)] = True
    rest_len = ntot-complete_len
    rest_waves = rest_len/nrep
    if not rest_waves.is_integer():
        raise ValueError("Data doesn't contain an interger number of packs")
    rest_waves = int(rest_waves)
    idx -= complete_len
    is_stab_waves[(idx>=0) & (idx % rest_waves < nstab)] = True
    return is_stab_waves

def spectra_idx(fn):
    p = r"spectrum_(?P<n>\d+)"
    n = int(re.match(p, fn).group("n"))
    return n
