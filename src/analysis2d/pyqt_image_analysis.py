# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 10:36:25 2015

@author: Helene
"""

# -*- coding: utf-8 -*-
"""
Demonstrates common image analysis tools.

Many of the features demonstrated here are already provided by the ImageView
widget, but here we present a lower-level approach that provides finer control
over the user interface.
"""
#import initExample ## Add path to library (just for examples; you do not need this)

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
#import pyqtgraph.opengl as gl
import numpy as np
import scipy as sp
from scipy import ndimage

pg.mkQApp()

class PyQtImageWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(PyQtImageWidget, self).__init__(parent) 

        self.setWindowTitle('pyqtgraph example: Image Analysis')


        self.pgGraph=pg.GraphicsLayout()
        self.layout = QtGui.QGridLayout()
        self.setLayout(self.layout)

        self.data= None
        self.x_axis = None
        self.y_axis = None
        self.z_axis= None
        self.axis_labels = {'x_axis': (('Excitation energy', 'eV')),'y_axis': (('Emission energy', 'eV')),
                            'z_axis': (('population time', 'fs')), 'intensity': (('Intensity', 'a.u.'))}

        self.trace_axes = {'bottom': IndexedAxis('bottom', mapping=self.x_axis),
                           'top': IndexedAxis('top', mapping=self.x_axis),
                           'left': IndexedAxis('left', mapping=self.y_axis),
                           'right': IndexedAxis('right', mapping=self.y_axis)}

        #Cut along x axis (Upper left plot)
        self.p1 =pg.PlotWidget()
        self.p1_title= ''
        self.p1.setTitle(self.p1_title)
        self.p1_unit=self.axis_labels['y_axis'][1]
        self.p1.setLabel('bottom', text=self.axis_labels['y_axis'][0], units=self.axis_labels['y_axis'][1])
        self.p1.setLabel('left', text=self.axis_labels['intensity'][0], units=self.axis_labels['intensity'][1])
        self.layout.addWidget(self.p1,0,0)

        #Main 2D plot (Upper right plot)
        pg_plot= pg.PlotItem(axisItems= self.trace_axes) #labels={'bottom': self.axis_labels['x_axis'], 'left': self.axis_labels['y_axis']},axisItems = self.trace_axes)
        self.p2= pg.ImageView(view=pg_plot)
        #idx= self.find_nearest(self.z_axis, self.p3.timeLine.value())
        self.plotitem_p2= self.p2.getView()
        self.p2_title= '' #self.p3_title + ' @ ' + '%.2f' % self.z_axis[idx] + ' [' + self.p3_unit + "]"
        #self.plotitem_p2.setTitle(self.p2_title)
        self.p2_unit=self.axis_labels['y_axis'][1]
        self.plotitem_p2.setLabel('bottom', text=self.axis_labels['x_axis'][0], units=self.axis_labels['x_axis'][1])
        self.plotitem_p2.setLabel('left', text=self.axis_labels['y_axis'][0], units=self.axis_labels['y_axis'][1])
        #self.p2.ui.menuBtn.hide()
        #self.p2.ui.roiBtn.hide()
        self.layout.addWidget(self.p2,0,1)
        self.img=self.p2.getImageItem()
        #self.p2.ui.roiBtn.click()
        # Contrast/color control
        self.hist = self.p2.getHistogramWidget()
        self.hist.gradient.loadPreset('thermal')
        self.hist.setImageItem(self.p2.getImageItem())
        # Isocurve drawing
        self.iso = pg.IsocurveItem(level=0.8, pen='g')
        self.iso.setParentItem(self.img)
        self.iso.setZValue(1)

        # Draggable line for setting isocurve level
        self.isoLine = pg.InfiniteLine(angle=0, movable=True, pen='g')
        self.hist.vb.addItem(self.isoLine)
        self.hist.vb.setMouseEnabled(y=False) # makes user interaction a little easier
        self.isoLine.setValue(0.8)
        self.isoLine.setZValue(1000) # bring iso line above contrast controls

        self.p3 = self.p2.getRoiPlot()
        self.p3.setMinimumHeight(100)
        self.p3.setLabel('bottom', text=self.axis_labels['z_axis'][0], units=self.axis_labels['z_axis'][1])
        self.p3.setLabel('left', text=self.axis_labels['intensity'][0], units=self.axis_labels['intensity'][1])
        self.p3_title = ''
        self.p3_unit = self.axis_labels['z_axis'][1]
        self.p3.setTitle(self.p3_title)
        self.layout.addWidget(self.p3, 1, 0)
        self.p2.timeLine.sigPositionChanged.connect(self.update_p2)


        #Cut along y axis (Bottom right plot)
        self.p4 = pg.PlotWidget()
        self.p4.setMaximumHeight(250)
        self.p4.setLabel('bottom', text=self.axis_labels['x_axis'][0], units=self.axis_labels['x_axis'][1])
        self.p4.setLabel('left', text=self.axis_labels['intensity'][0], units=self.axis_labels['intensity'][1])
        self.p4_title=''
        self.p4_unit=self.axis_labels['x_axis'][1]
        self.p4.setTitle(self.p4_title)
        self.layout.addWidget(self.p4, 1, 1)

        self.generate_data()
        #self.p1.play(2)
        # Custom ROI for selecting an image region (zoom)
        #self.roi = pg.ROI([-8, 14], [6, 5])
        #print min(self.data[:,1])
        #print min(self.data[:,1]) + 20
        #self.roi=pg.ROI([min(self.data[:,1,0]),min(self.data[1,:,0])], [min(self.data[:,1,0]) +20, min(self.data[1,:,0]) + 20])
        #self.roi.addScaleHandle([0.5, 1], [0.5, 0.5])
        #self.roi.addScaleHandle([0, 0.5], [0.5, 0.5])
        #self.p1.addItem(self.roi)
        #self.roi.setZValue(10)  # make sure ROI is drawn above image


        self.line1= self.add_datacursor("y", True)
        self.line1.setPos(0)
        self.line1.setPen(color=QtGui.QColor(0, 128, 255, 255), width=3)
        self.line1.setHoverPen('b', width=3)
        self.line2= self.add_datacursor("x", True)
        self.line2.setPos(0)
        self.line2.setPen(color=QtGui.QColor(102, 255, 102, 255), width=3)
        self.line2.setHoverPen(color=QtGui.QColor(0, 102, 0, 255), width=3)


        self.line1.sigPositionChanged.connect(self.update_p1)
        self.line2.sigPositionChanged.connect(self.update_p4)
        #self.roi.sigRegionChanged.connect(self.updateImg1b)
        #self.isoLine.sigDragged.connect(self.updateIsocurve)
        #self.hist.sigMouseReleased(self.updateZoomedHist)

        #self.updatePlot1()
        #self.updatePlot2()
        #self.updateImg1b()
        self.resize(800, 800)
        self.show()

    def update_labels_units(self, new_dict):
        self.axis_labels= new_dict
        #Updates p1 plot
        self.p1_unit=self.axis_labels['y_axis'][1]
        self.p1.setLabel('bottom', text=self.axis_labels['y_axis'][0], units=self.axis_labels['y_axis'][1])
        self.p1.setLabel('left', text=self.axis_labels['intensity'][0], units=self.axis_labels['intensity'][1])
        self.p1_title= self.axis_labels['x_axis'][0]
        self.p1.setTitle(self.p1_title)
        #Updates p2 plot
        self.p2_unit = self.axis_labels['y_axis'][1]
        self.plotitem_p2.setLabel('bottom', text=self.axis_labels['x_axis'][0], units=self.axis_labels['x_axis'][1])
        self.plotitem_p2.setLabel('left', text=self.axis_labels['y_axis'][0], units=self.axis_labels['y_axis'][1])
        #Updates p3 plot
        self.p3_unit = self.axis_labels['z_axis'][1]
        self.p3.setLabel('bottom', text=self.axis_labels['z_axis'][0], units=self.axis_labels['z_axis'][1])
        self.p3.setLabel('left', text=self.axis_labels['intensity'][0], units=self.axis_labels['intensity'][1])
        #Updates p4 plot
        self.p4_unit = self.axis_labels['x_axis'][1]
        self.p4.setLabel('bottom', text=self.axis_labels['x_axis'][0], units=self.axis_labels['x_axis'][1])
        self.p4.setLabel('left', text=self.axis_labels['intensity'][0], units=self.axis_labels['intensity'][1])
        self.p4_title= self.axis_labels['y_axis'][0]
        self.p4.setTitle(self.p4_title)

    def update_axis(self, x_axis, y_axis, z_axis):
        self.x_axis= x_axis
        print "x_axis"
        print self.x_axis

        self.y_axis= y_axis
        print "y_axis"
        print self.y_axis
        self.z_axis = z_axis
        self.line1.setPos(self.y_axis[0])
        self.line2.setPos(self.x_axis[0])
        self.p2.roi.setPos( (self.x_axis[0], self.y_axis[0]))

        #self.scale_x= (self.x_axis[-1] - self.x_axis[0])#/np.size(self.data, axis=1)
        #self.scale_y= (self.y_axis[-1] - self.y_axis[0]) #/np.size(self.data, axis=2)

        for a in (self.trace_axes[k] for k in ['top', 'bottom']):
            print "going in mapping x"
            a.mapping = self.x_axis
            a.force_update()
        for a in (self.trace_axes[k] for k in ['left', 'right']):
            print "going in mapping y"
            a.mapping = self.y_axis
            a.force_update()

    def plot_data(self, data):
        self.data=np.swapaxes(data,1,2)
        print "shape of data before plotting"
        print np.shape(self.data)
        print np.shape(self.x_axis)
        print np.shape(self.y_axis)
        print np.shape(self.z_axis)
        print np.swapaxes(self.data, 1, 2).shape
        self.p2.setImage(self.data, xvals=self.z_axis)
        self.hist.setLevels(self.data.min(), self.data.max())

        # build isocurves from smoothed data
        #self.iso.setData(pg.gaussianFilter(self.data[self.p1.currentIndex, :, :], (2, 2)))


    def generate_data(self):
        # Generate image data

        img = ndimage.gaussian_filter(np.random.normal(size=(200, 200)), (5, 5)) * 20 + 100
        img = img[np.newaxis,:,:]
        decay = np.exp(-np.linspace(0,0.3,100))[:,np.newaxis,np.newaxis]
        self.data = np.random.normal(size=(100, 200, 200))
        self.data += img * decay
        self.data += 2

        ## Add time-varying signal
        sig = np.zeros(self.data.shape[0])
        sig[30:] += np.exp(-np.linspace(1,10, 70))
        sig[40:] += np.exp(-np.linspace(1,10, 60))
        sig[70:] += np.exp(-np.linspace(1,10, 30))

        sig = sig[:,np.newaxis,np.newaxis] * 3
        self.data[:,50:60,50:60] += sig
        #self.p1.axes.update()
        self.x_axis= np.arange(len(self.data[0,:,1]))
        self.y_axis= np.arange(len(self.data[0,1,:]))
        self.z_axis= np.linspace(1., 3., self.data.shape[0])
        ## Display the data and assign each frame a time value from 1.0 to 3.0
        self.p2.setImage(self.data, pos=[0, 0], scale=[10, 10], xvals=np.linspace(1., 3., self.data.shape[0]),
                         autoHistogramRange=True)
        self.p2.autoRange()

    def add_datacursor(self, axis, movable):
            if axis == "y":
                line = pg.InfiniteLine(angle=0, movable=True)
            elif axis == "x":
                line = pg.InfiniteLine(angle=90, movable=True)
            #Adds line to 2D map plot
            self.p2.addItem(line)

            return line
     # small method that returns the index of the element in an array that is closest
     # to "value'
    def find_nearest(self,array,value):
        idx = (np.abs(array-value)).argmin()
        return idx

    # Callbacks for handling user interaction
    def update_p1(self):
        #selected = roi.getArrayRegion(data, img)
        line1_pos= self.line1.value()
        print "LINE1_pos"
        print line1_pos
        idx_dim3=self.p2.currentIndex
        #yaxis= np.arange(len(self.data[idx_dim3,1,:]))
        idx=self.find_nearest(np.array(self.y_axis), line1_pos)
        #xaxis=np.arange(len(self.data[idx_dim3,:,idx]))
        title= self.p1_title + ' @ ' + '%.2f' % self.y_axis[idx] + ' ' + self.p1_unit
        self.p1.setTitle(title)
        self.p1.plot(np.array(self.x_axis),self.data[idx_dim3,:,idx], clear=True, pen=QtGui.QColor(0, 128, 255, 255), width=3)
        #p2.plot(selected.mean(axis=1), clear=True)

    def update_p2(self):
        idx= self.find_nearest(self.z_axis, self.p2.timeLine.value())
        self.plotitem_p2.setTitle(title= self.p3_title + ' @ ' + '%.2f' % self.z_axis[idx] + ' [' + self.p3_unit + ']')
        #self.plot_data()
    def update_p4(self):
        #selected = roi.getArrayRegion(data, img)
        line2_pos= self.line2.value()
        idx_dim3= self.p2.currentIndex

        #yaxis= np.arange(len(self.data[:,1,idx_dim3]))
        idx=self.find_nearest(self.x_axis, line2_pos)
        #xaxis=np.arange(len(self.data[idx,:,idx_dim3]))

        title= self.p4_title + ' @ ' + '%.2f' % self.x_axis[idx] + ' ' + self.p4_unit
        self.p4.setTitle(title)
        self.p4.plot(np.array(self.y_axis),self.data[idx_dim3,idx,:], clear=True, pen=QtGui.QColor(102, 255, 102, 255), width=3)


    def set_axis_properties(self, plt_idx):
        pen = pg.mkPen(color=(255,255,255), width=1.5)
        #self.plot_list[plt_idx].getAxis('left').setPen(pen)
        #self.plot_list[plt_idx].getAxis('bottom').setPen(pen)

class IndexedAxis(pg.AxisItem):
    """
    Axis where pixels are mapped using a numpy array
    """
    def __init__(self, orientation, mapping=None, **kw):
        super(IndexedAxis, self).__init__(orientation, **kw)
        self.mapping = mapping
        self.fmt = "{:.02f}"

    def tickStrings(self, values, scale, spacing):
        if self.mapping is None:
            return [self.fmt.format(v) for v in values]
            #return super(IndexedAxis, self).tickStrings(values, scale, spacing)
        # count values smaller than 0
        labels = []
        idx = np.array(values, dtype=np.int)-1
        left_pad = np.count_nonzero(idx < 0)
        right_pad = np.count_nonzero(idx >= self.mapping.size)
        idx = np.compress(np.logical_and(idx>=0, idx< self.mapping.size),
                          idx)
        labels.extend([""]*left_pad)
        labels.extend([self.fmt.format(v) for v in self.mapping[idx]])
        labels.extend([""]*right_pad)
        return labels

    def force_update(self):
        self.picture = None
        self.update()


## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    #if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
    #    QtGui.QApplication.instance().exec_()
    """
    app = QtGui.QApplication(sys.argv)
    test=PyQtImageWidget()
    test.set_x_label(0, 'Test')
    test.set_y_label(0, 'Test')
    test.set_title(0,'Title')
    sys.exit(app.exec_())
    """