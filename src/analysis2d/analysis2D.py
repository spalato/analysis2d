__author__ = 'Helene_Seiler'

"""
This is the Main Widget for plotting, visualizing and analyzing 2D data
"""
from pyqtgraph.Qt import QtCore, QtGui
import os.path as pth
import numpy as np
from ConfigParser import ConfigParser
import scipy as sp
from scipy import constants
import phasecycling as pc
from glob import glob
import pyqt_image_analysis as plot_hub
from scipy.interpolate import interp1d
from PyQt4 import QtGui, QtCore

AXIS_LABELS = {'x_axis': (('Excitation energy', 'eV')), 'y_axis': (('Emission energy', 'eV')),
                             'z_axis': (('population time', 'fs')), 'intensity': (('Intensity', 'a.u.'))}

z_axis_values= 585 + np.array([-520, -515, -510, -505, -500, -495, -490, - 485, -480, -475, -470, -465, -460, -445, -430, -415, -400,
                -385, -370, -355, -340])
print z_axis_values
class Experiment(QtGui.QWidget):

    def __init__(self, path="Y:/GROUP/experimental/data/2D/080814/2D/RUN18/*csv", parent=None):

        super(Experiment, self).__init__(parent)


        #Everything that has to do with the Menu
        self.myQMenuBar = QtGui.QMenuBar(self)

        exitMenu = self.myQMenuBar.addMenu('File')
        exitAction = QtGui.QAction('Exit', self)
        exitAction.triggered.connect(QtGui.qApp.quit)
        exitMenu.addAction(exitAction)

        loadAction = QtGui.QAction('Load files', self)
        loadAction.triggered.connect(self.set_up_exp)
        exitMenu.addAction(loadAction)


        self.plot_hub=plot_hub.PyQtImageWidget()

        #Vlayout is the main layout
        self.Vlayout = QtGui.QVBoxLayout()
        self.Vlayout.addWidget(self.myQMenuBar)
        self.Vlayout.addWidget(self.plot_hub)
        self.setLayout(self.Vlayout)

        #Window properties are set here
        self.setGeometry(510, 500, 900, 700)
        self.setWindowTitle('2D Analysis Software')
        QtGui.QApplication.setStyle("windowsxp")

        self.show()


    def set_up_exp(self):
        self.data_mean = []
        self.load_file_dialog= QtGui.QFileDialog()
        for path in QtGui.QFileDialog.getOpenFileNames(self):
            print path
            self.data_mean.append(np.loadtxt(str(path), delimiter=None))

        self.data_mean=np.array(self.data_mean)
        self.x_axis_values= self.data_mean[:,0,1:]
        self.y_axis_values = self.data_mean[:, 1:, 0]
        print "shape of data-mean, x, y"
        print np.shape(self.x_axis_values[0,:])
        print self.x_axis_values[0,:]
        print np.shape(self.y_axis_values[0,:])
        print np.shape(z_axis_values)
        print np.shape(self.data_mean)
        self.plot_hub.update_axis(np.squeeze(self.x_axis_values[0,:]),np.squeeze(self.y_axis_values[0,:]), z_axis_values)
        self.plot_hub.update_labels_units(AXIS_LABELS)
        print "SHAPE OF DATA"
        self.data= self.data_mean[:,1:,1:]
        print np.shape(self.data)
        self.plot_hub.plot_data(self.data)

    def change_x_axis(self):
        self.x= str(self.chose_x.currentText())
        self.plot_hub.set_axis(self.list_axis[self.x], self.list_axis[self.y], self.list_axis[self.z])
        #perform fourier transform if needed
        #converts to energy if needed
        self.plot_hub.plot_data(self.data_mean)

    def change_y_axis(self):
        self.y= str(self.chose_y.currentText())
        self.plot_hub.set_axis(self.list_axis[self.x], self.list_axis[self.y], self.list_axis[self.z])
        self.plot_hub.plot_data(self.data_mean)

    def change_z_axis(self):
        self.z= str(self.chose_z.currentText())
        self.plot_hub.set_axis(self.list_axis[self.x], self.list_axis[self.y], self.list_axis[self.z])
        self.plot_hub.plot_data(self.data_mean)



if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)

    path_test="Y:/GROUP/experimental/data/2D/080814/2D/RUN5/*csv"
    test=Experiment(path=path_test)
    sys.exit(app.exec_())

    """
    plot_hub=plot_hub.PyQtImageWidget()
    #plot_hub.set_x_label(1,'Test')
    #plot_hub.set_title(1,'Test')

    list_axis = {'wavelength': ['wavelength', 'nm', test.wavelengths],
                 't1': ['t1', 'fs', test.time_arrays[0]],
                 't2': ['t2', 'fs', test.time_arrays[1]],
                 'f1': ['f1', 'THz', test.frequency_arrays[0]],
                 'f2': ['f2', 'THz', test.frequency_arrays[1]],
                 'f3': ['f3', 'THz', test.frequency_arrays[2]],
                 'E1': ['E1', 'eV', test.energy_arrays[0]],
                 'E2': ['E2', 'eV', test.energy_arrays[1]],
                 'E3': ['E3', 'eV', test.energy_arrays[2]]}

    plot_hub.set_axis(list_axis['t1'], list_axis['wavelength'], list_axis['t2'])

    ft_array=test.perform_fft(0)

    plot_hub.plot_data(test.data_mean)
    sys.exit(app.exec_())
    #test.load_config('settings.cfg')
    """