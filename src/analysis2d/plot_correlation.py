# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 23:01:07 2015

@author: Helene
"""

import numpy as np
import scipy as sp
from scipy import constants
from scipy.interpolate import interp1d
import os
import matplotlib.pylab as plt
from scipy.fftpack import fftfreq, fftshift
import pickle

cwd= os.getcwd()

mean_data= np.loadtxt( cwd + '\mean_array.txt', delimiter=' ', dtype=float)
#need to delete first two columns, at least in the formatting given by Winspec
mean_data=np.delete(mean_data, [0,1], axis=1)

wavelengths=np.loadtxt(cwd + '.\wavelengths_acton.txt', dtype=float)
t1_ini=0
t1_final= 100.8
coherence_time = np.linspace(t1_ini, t1_final, 252, endpoint= False)

freq_temp= np.divide(sp.constants.c, wavelengths*1e-9)/1e12
jacobian= np.divide(sp.constants.c, np.power(freq_temp, 2))
len_freq_temp=len(freq_temp)
a=freq_temp[0]
b=freq_temp[-1]
#Converts to THz
f3=np.linspace(a,b,len_freq_temp)
E3= (sp.constants.h/sp.constants.e) * f3*1e12
E3=E3[::-1]
#energy_arrays.append((sp.constants.h/sp.constants.e) * self.frequency_arrays[-1]*1e12)

data_p1= mean_data[::2]
data_p1= data_p1*jacobian
#apply jacobian
data_p2= mean_data[1::2]
data_p2= data_p2*jacobian

data_p = data_p1 - data_p2

f1 = interp1d(freq_temp, data_p1)
f2= interp1d(freq_temp,data_p2)

data_p1_interp= f1(f3)
data_p2_interp= f2(f3)

data_interp = data_p1_interp - data_p2_interp
fig1, ax1= plt.subplots(1,1)
plt.plot(freq_temp, f1(freq_temp)[1,:], 'o', f3, f1(f3)[1,:], '-')



"""
        freq_temp= np.divide(sp.constants.c, self.wavelengths*1e-9)/1e12
        freq_temp= freq_temp[::-1]

        print freq_temp
        print self.frequency_arrays[-1][::-1]
        f = interp1d(freq_temp, array)

        interpolated_data = f(self.frequency_arrays[-1][::-1])

        print np.shape(interpolated_data)
        return interpolated_data
"""

nfft= 10*len(coherence_time)


ft_array= np.fliplr(fftshift(np.abs(np.fft.fft(data_interp, nfft, axis=0)), axes=(0,)))
f1 = fftfreq(nfft,0.4*1e-15)
f1= fftshift(f1)
#f1= np.arange(-nfft/2, nfft/2,1)*(1/(0.4*1e-15))/nfft
#f1= f1*1e-12
E1= (sp.constants.h/sp.constants.e) * f1


print np.shape(np.transpose(np.abs(ft_array)))
f, ax= plt.subplots(1,1)
#ax.imshow(np.abs(ft_array), aspect= 'auto', cmap=plt.get_cmap('jet') )# , extent=[E3[0],E3[-1],E1[0],E1[-1]])
#

ax.imshow(np.abs(ft_array[1750:1805,125:260]), aspect= 'auto', cmap=plt.get_cmap('jet') , extent=[E3[125],E3[260],E1[1750],E1[1805]])
# Plotting part
axis_lw=2
plt.rcParams['axes.linewidth'] = axis_lw
#fontsize
fs=20
#labelpad
lp= 15
f2, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, sharex= True)
f2.subplots_adjust(hspace=0)


f4, ax4= plt.subplots(1,1)
ax1.imshow(np.fliplr(np.transpose(data_p[:,800:1200])), aspect ='auto', cmap= plt.get_cmap('jet') , extent= [coherence_time[0], coherence_time[-1],wavelengths[1200], wavelengths[800]])
#ax1 properties

#ax1.imshow(data_interp[:,800:900], aspect='auto', cmap=plt.get_cmap('jet') , extent=[coherence_time[0],coherence_time[-1], wavelengths[800],wavelengths[900] ])
ax1.set_ylabel('wavelengths [nm]', size= fs)
ax1.xaxis.labelpad= lp
ax1.yaxis.labelpad = lp
plt.tick_params(axis='both', which='major', labelsize=fs, width= 2, length=8)
#ax2 properties
ax2.plot(coherence_time, (data_p[:,940]/np.max(data_p[:,940]))[::-1])

ax2.set_xlabel('coherence time [femtosecond]', size=fs)
ax2.set_ylabel('signal intensity [a.u]', size=fs)
ax2.set_xlim([t1_ini, t1_final])
ax2.xaxis.labelpad= lp
ax2.yaxis.labelpad = lp
plt.tick_params(axis='both', which='major', labelsize=fs, width= 2, length=8)

plt.show()



#pickling

output = open('correlation_data.pkl', 'wb')
correlation_map= np.abs(ft_array[1750:1805,125:260])
x_axis= E1[1750:1805]
y_axis= E3[125:260]
unit_x = 'eV'
unit_y='eV'
correlation_data={'correlation map': correlation_map, 'x axis': x_axis, 'y axis': y_axis, 'unit x axis': unit_x, 'unit y axis': unit_y}
# Pickle dictionary using protocol 0.
pickle.dump(correlation_data, output)

output.close()

output2= open('time_series.pkl', 'wb')

time_series={'time array': coherence_time, 'signal array': (data_p[:,940]/np.max(data_p[:,940]))[::-1], 'time axis label': 'coherence time [fs]', 'signal axis label': 'intensity [a.u]'}

np.savetxt('coherence_time.txt', coherence_time)
np.savetxt('signal_array.txt',(data_p[:,940]/np.max(data_p[:,940]))[::-1] )
np.savetxt('correlation_map.txt', correlation_map)
np.savetxt('x_axis_cmap.txt', x_axis)
np.savetxt('y_axis_cmap.txt', y_axis)
pickle.dump(time_series, output2)
output2.close()
