import argparse

parser = argparse.ArgumentParser(description="Fooling with exclusive groups")

ex_shift = parser.add_mutually_exclusive_group(required=True)
ex_shift.add_argument("-m", "--manual", action="store", nargs=2, type=float,
                      help="Central wavelength (nm) and user frame shift (PHz)")
ex_shift.add_argument("-s", "--settings", action="store",
                      help="Settings file")


# # Doesn't work
# ex_shift = parser.add_mutually_exclusive_group(required=False)
# manual_group = ex_shift.add_argument_group("Manual specification")
# manual_group.add_argument("-c", "--cwl", action="store", type=float, required=True,
#                     help="Central wavelength.")
# manual_group.add_argument("-u", "--user", action="store", type=float, required=True,
#                     help="User frame shift.")
# ex_shift.add_argument("-s", "--settings", action="store",
#                       help="Settings file")

# man_grp = parser.add_argument_group("manual")
# man_grp.add_argument("-c", "--cwl", action="store", type=float, required=True,
#                     help="Central wavelength.")
# man_grp.add_argument("-u", "--user", action="store", type=float, required=True,
#                     help="User frame shift.")



parser.add_argument("file",
                    help="Data as .npz archive.")
args = parser.parse_args()#["-c600", "-u0.1", "data.npz"])
print(args)