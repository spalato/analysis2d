Ancilliary tools
================
This library comes with a few scripts that help in handling the 2D data, but
do not process things themselves. This HOWTO covers the setup (on LINUX) and
usage. Examples from the command line are given.

Setup
=====
To be able to launch scripts from the command line, the scripts should be stored
in a custom directory somewhere on your `PATH`, and be made executable.

The steps required to change the `PATH` environment variables are covered in
[HOWTO_NAS.md]. Basically, you need to edit your `~/.bashrc` file. A common
choice is `~/bin`.

Any scripts in this `bin` folder can be executed without specifying the complete
path. For this, two conditions must be met: the file must begin with a *shebang*
indicating which interpreter to use (ie: python vs bash vs ...), and be made
executable using `chmod`. Again, refer to [HOWTO_NAS.md].

For maximal convenience, you can use symlinks to the scripts, so the behavior
is updated whenever the git repo is updated. To do this, run the follwing
command from the `bin` folder.
```bash
ln -s absolute_source_path link_name
``` 
This will create a symlink. Note that extensions don't matter. You then need to 
make the link executable using `chmod`.

If you followed the instructions and copied my `bin` folder, it should already
be populated with a bunch of well-behaved symlink. To try it out, just navigate
anywhere (away from `~/bin`) and try running `npz_shape` without any args. It
should complain about missing arguments.


[HOWTO_NAS.md]:HOWTO_NAS.md#executing-things-painlessly---environment-variables.

Usage
=====

This section covers the usage of the tools, with examples. Note that they are
likely to be compatible between python 2 and 3.

npz_shape
-----------
Prints information about an `.npz` archive. Simply prints the file name, array
names, shape and dtype. Multiple names can be supplied, the results are simply
printed consecutively. Note that loading these large archives results in a
noticeable delay.
 
Example:
```
$ npz_shape trial_05_data_mean.npz
trial_05_data_mean.npz :
    wl     (670,)          float64
    t2     (82,)           float64
    t1     (40,)           float64
    stab   (82, 670)       float64
    coh    (40, 82, 670)   float64
    data   (160, 82, 670)  float64
```

saturation
------------
Write a report about staturated pixels. Contains a report in the
header and saturated pixel counts as an array that can be loaded using
`numpy.loadtxt`.

The script expects a list of sequence files. It produces an array where the lines
correspond to the different files and the columns to index of the wave in the
`sequence_rep_ZZZ.npy` file (ie: in the order acquired by the spectro).

The count contains the number of saturated pixel in each spectrum. A header
summarizing the number of saturated data, stab and total waves is included.
Refer to the code for specific thresholds.

Example, from a spectrum folder:
```
$ saturation ../settings.cfg sequence_rep*.npy
INFO     Loading settings: ../settings.cfg
INFO     Saving to: saturations.txt
$ grep "#" saturations.txt
# {
#   "Total rejected waves": 0,
#   "Total rejected data": 0,
#   "Rejected reps": [],
#   "n_files": 10,
#   "Total rejected stab": 0
# }
```

Potentially more useful, from a trial folder:
```bash
for spec in $(ls -d spectrum_*/)
do
    saturation -o ${spec%/}_saturation.txt settings.cfg ${spec}/sequence_rep*.npy
done
```

The script takes a few arguments, have a look the help.

sorted_spectra
----------------
Sort a list of file paths by spectrum idx. This was mostly a test. Initially
spectra were not left-padded with 0s. As such, native bash order would yield:
`0 1 10 100 101 102 ... 109 11 110 ...` and so on. This is lexical ordering.
This scripts just reorders them by number as you would expect (`0 1 2 3 ...`)
and prints to the console output.

Example:
```bash
sorted_spectra spectrum*/data_mean.txt > sorted_data_paths.txt
```

summarize
-----------
Produce a summary from a collection of settings file. After a long day and
night of experiments, there usually are many pump-probe and 2d trials. 
Typically, only a few are good long runs that we want to analyze. This script
helps in sorting things out by writing summary information to a text file.

Example, from a date folder (ie: 18-02-23):
```
$ summarize */*/settings.cfg
$ cat exp_summary.txt
path            type                    n_rep   n_spec  comment
---------------------------------------------------------------
2d/trial_0      2DE_NC                  60      1       checking powers
2d/trial_1      2DE_NC                  60      1       quick 2d - NB
2d/trial_2      2DE_NC                  60      1       quick 2d - NB, added 0.1 od filter
2d/trial_3      2DE_NC                  300     31      2d Toluene, pre experiment
2d/trial_4      2DE_NC                  300     1       quick 2d - DS24
2d/trial_5      2DE_NC                  300     80      DS24 pop time study
2d/trial_6      2DE_NC                  300     1       DS24 test, less power
2d/trial_7      2DE_NC                  300     80      DS24 pop time study, less power
2d/trial_8      2DE_NC                  300     240     DS24 pop time study, more power
pp/trial_0      PumpProbe               60      1       finding signal - NB
pp/trial_1      PumpProbe               60      1       optmising signal - NB
pp/trial_2      PumpProbe               240     1       good t0 - NB
pp/trial_3      PumpProbe               60      1       checking powers
pp/trial_4      PumpProbe               300     1       finding signal - DS24
pp/trial_5      PumpProbe               300     1       good pp run - DS24
pp/trial_6      PumpProbe               300     1       after 1st 2d run - DS24
pp/trial_7      PumpProbe               300     1       after 2nd 2d run - DS24
```
