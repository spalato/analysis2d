from setuptools import setup, find_packages

setup(name='analysis2d',
      version='0.1',
      description='2d analysis tools',
      url='',
      author='H. Seiler, S. Palato',
      author_email='',
      license='',
      packages=find_packages("src"),
      package_dir={"": "src"},
      zip_safe=False)